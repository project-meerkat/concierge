{% load i18n %}{% load settings_value %}{% blocktrans %}Dear {{ name }},{% endblocktrans %}

{% blocktrans %}At your request we send you a link to create a new password at {{ site_name }}.{% endblocktrans %}

{% blocktrans %}Click the following link or copy the link to your browser to create a new password.{% endblocktrans %}

{{ reset_url }}

{% blocktrans %}This link is valid until {{ due_date }}.{% endblocktrans %}
