import logging
from functools import wraps

from django.http import HttpResponseForbidden
from oidc_provider.models import Client

logger = logging.getLogger(__name__)


class NoAccessError(Exception):
    pass


def assert_pleio_subsite_access(request):
    try:
        client_id = request.headers["x-oidc-client-id"]
        secret = request.headers["x-oidc-client-secret"]

        client = Client.objects.get(client_id=client_id)
        assert client.client_secret == secret, "Not allowed"
    except (AssertionError, KeyError, Client.DoesNotExist):
        raise NoAccessError()


def require_valid_pleio_origin(view_func):
    @wraps(view_func)
    def _wrapped_view_func(request, *args, **kwargs):
        try:
            assert_pleio_subsite_access(request)
            return view_func(request, *args, **kwargs)
        except NoAccessError:
            return HttpResponseForbidden()

    return _wrapped_view_func
