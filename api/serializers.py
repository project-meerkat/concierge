from rest_framework import serializers

from core.models import EventLog, User


class UserSerializer(serializers.ModelSerializer):
    guid = serializers.IntegerField(source="id")
    isAdmin = serializers.BooleanField(source="is_superuser")  # noqa: N815
    isSmoothoperator = serializers.BooleanField(source="is_smoothoperator")  # noqa: N815
    avatarUrl = serializers.CharField(source="avatar_url")  # noqa: N815

    class Meta:
        model = User
        fields = (
            "guid",
            "username",
            "name",
            "picture",
            "avatarUrl",
            "email",
            "isAdmin",
            "isSmoothoperator",
        )


class AccountDeletedSerializer(serializers.ModelSerializer):
    class Meta:
        model = EventLog
        fields = ["userid", "event_time", "event_type"]
