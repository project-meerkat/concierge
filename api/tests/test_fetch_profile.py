import os.path
import uuid
from unittest import mock

from django.core.files.base import ContentFile
from mixer.backend.django import mixer
from oidc_provider.models import Client

from core.avatar_processing import Avatar
from core.models import OriginSite, UserSiteRegistration
from core.tests import TestCase


class TestFetchProfileTestCase(TestCase):
    FETCH_PROFILE = "/api/users/fetch_profile/%s"

    def setUp(self):
        super().setUp()

        self.profile = mixer.blend(UserSiteRegistration, origin_token=uuid.uuid4())
        self.oidc_client = mixer.blend(Client)
        self.user = self.profile.user
        self.user.avatar = ContentFile(
            open(os.path.join(os.path.dirname(__file__), "assets", "face.jpg"), "rb").read(),
            "face.jpg",
        )
        self.user.save()
        self.user.process_avatar()
        self.avatar = Avatar(self.user)

    def tearDown(self):
        OriginSite.objects.all().delete()
        super().tearDown()

    def authorized_request(self, user_id):
        return self.client.get(
            self.FETCH_PROFILE % user_id,
            HTTP_X_OIDC_CLIENT_ID=self.oidc_client.client_id,
            HTTP_X_OIDC_CLIENT_SECRET=self.oidc_client.client_secret,
        )

    @mock.patch("logging.Logger.warning")
    def test_fetch_user_without_authorization_serves_403(self, mocked_log_warning):
        response = self.client.get(self.FETCH_PROFILE % self.user.id)
        self.assertEqual(response.status_code, 403)

    @staticmethod
    def get_non_existent_user():
        profile = mixer.blend(UserSiteRegistration, origin_token=uuid.uuid4())
        try:
            return profile.user.id
        finally:
            profile.user.delete()

    @mock.patch("logging.Logger.warning")
    def test_fetch_non_existent_user_serves_404(self, mocked_log_warning):
        response = self.authorized_request(self.get_non_existent_user())
        self.assertEqual(response.status_code, 404)

    def test_fetch_serves_user(self):
        # When
        response = self.authorized_request(self.user.id)
        data = response.json()

        # Then
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data["guid"], self.user.id)
        self.assertEqual(data["username"], self.user.username)
        self.assertEqual(data["name"], self.user.name)
        self.assertEqual(data["email"], self.user.email)
        self.assertEqual(data["isAdmin"], self.user.is_superuser)
        self.assertEqual(data["isSmoothoperator"], self.user.is_smoothoperator)
        self.assertIn(self.avatar.crc_filename(), data["avatarUrl"])

    def test_fetch_no_avatar(self):
        self.user.avatar = ""
        self.user.save()

        # When
        response = self.authorized_request(self.user.id)
        data = response.json()

        # Then
        self.assertEqual(response.status_code, 200)
        self.assertIn("/static/avatars/default/large.jpg", data["avatarUrl"])
