import uuid
from unittest import mock

from mixer.backend.django import mixer
from oidc_provider.models import Client

from core.models import OriginSite, User, UserSiteRegistration
from core.tests import TestCase


class TestRegistrationTestCase(TestCase):
    REGISTER_ORIGIN_SITE = "/api/users/register_origin_site/%s"

    def setUp(self):
        super().setUp()

        self.user = mixer.blend(User)
        self.oidc_client = Client.objects.create(client_id="123456", client_secret=str(uuid.uuid4()))
        self.SITE_URL = "https://demo.net"
        self.SITE_NAME = "Just a test"
        self.SITE_DESCRIPTION = "Just some description"
        self.ORIGIN_TOKEN = str(uuid.uuid4())

    def tearDown(self):
        OriginSite.objects.all().delete()
        super().tearDown()

    @mock.patch("django.views.generic.base.logger.warning")
    def test_register_gives_forbidden_when_not_authenticated(self, mocked_warning):
        response = self.client.post(self.REGISTER_ORIGIN_SITE % self.user.id)

        self.assertEqual(response.status_code, 403)

    @mock.patch("django.views.generic.base.logger.warning")
    def test_register_requires_complete_data(self, mocked_warning):
        response = self.client.post(
            self.REGISTER_ORIGIN_SITE % self.user.id,
            HTTP_X_OIDC_CLIENT_ID=self.oidc_client.client_id,
            HTTP_X_OIDC_CLIENT_SECRET=self.oidc_client.client_secret,
        )
        self.assertEqual(response.status_code, 400)

    @mock.patch("core.tasks.refresh_site_favicon.delay")
    def test_register_produces_new_instances(self, mocked_refresh_site_icon):
        response = self.client.post(
            self.REGISTER_ORIGIN_SITE % self.user.id,
            data={
                "origin_site_url": self.SITE_URL,
                "origin_site_name": self.SITE_NAME,
                "origin_site_description": self.SITE_DESCRIPTION,
                "origin_token": self.ORIGIN_TOKEN,
            },
            HTTP_X_OIDC_CLIENT_ID=self.oidc_client.client_id,
            HTTP_X_OIDC_CLIENT_SECRET=self.oidc_client.client_secret,
        )
        # OK signal?
        self.assertEqual(response.status_code, 200)

        # created origin site?
        origin_site = OriginSite.objects.get(origin_site_url=self.SITE_URL)
        self.assertEqual(origin_site.origin_site_name, self.SITE_NAME)

        # created user reference?
        relation = UserSiteRegistration.objects.get(user=self.user, origin_token=self.ORIGIN_TOKEN)
        self.assertEqual(relation.origin_site, origin_site)

        # Scheduled refresh_site_icon?
        self.assertTrue(mocked_refresh_site_icon.called)
