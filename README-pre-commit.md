# Git hooks

Git hooks are scripts that git executes when specific Git operations take place.
To give an example, when you do a "git push", git calls the "pre-push" hooks
before it does the actual push.

The nice thing is that you, the user of git, can provide these scripts. To give
an example, if you provide a pre-push hook that calls flake8 on the changed
files, git will abort the push when flake8 finds issues. Have you ever waited on
a CI build to complete only to find out it has aborted due to a flake8 warning?
That's a scenario of the past when you have such a pre-push hook in place.

This repo uses Python package [pre-commit](https://pre-commit.com/) to configure
the hooks using the YAML file ``.pre-commit-config.yaml`` in the root of this
repo. pre-commit not only makes it easy to install and deinstall hooks, it also
provides a library of predefined hooks that you can use.

Currently the pre-commit YAML file configures the following hooks:

ruff
: reformat code according to PEP8 rules, and check for code issues

All these hooks are pre-push hooks: they are executed just before the actual
push takes place. If one of the hooks encounters improperly formatted code, the
push will abort.

## Hooks in action

Let's see a git hook in action. Suppose the flake8 hook is installed. If you try
to push a file that is not flake8-compliant, the git output will look something
like this::

    (venv) $ git push origin HEAD
    flake8...................................................................Failed
    - hook id: flake8
    - exit code: 1

    src/apps/planner/views.py:38:9: F841 local variable 'hello' is assigned to but never used

    error: failed to push some refs to 'gitlab.com:pleio/dossier.git'
    (venv) $

When you've just installed the flake8 hook and execute git push for the first
time, pre-commit will download flake8 from that repo and install it for you::

    (venv) $ git push origin HEAD
    [INFO] Initializing environment for https://github.com/pycqa/flake8.
    [INFO] Installing environment for https://github.com/pycqa/flake8.
    [INFO] Once installed this environment will be reused.
    [INFO] This may take a few minutes...

    flake8...................................................................Failed
    - hook id: flake8
    - exit code: 1

    src/apps/planner/views.py:38:9: F841 local variable 'hello' is assigned to but never used

That download and installation is only done once. Note that this installation of
flake8 takes place in its own virtualenv. It won't pollute your development
virtualenv or the global Python installation.

Your development virtualenv already provides flake8 and it is possible to
configure the hook to use that installation of flake8 instead of downloading it.
However this does not always play nice with an IDE. For example, PyCharm until
2012.2 could not run the Git hooks in the development virtualenv.

## (De-)installation of hooks

You have to install the hooks before they become active. To install all pre-push
hooks you execute::

    (venv) $ pre-commit install --hook-type pre-push

To uninstall the pre-push hooks you execute::

    (venv) $ pre-commit uninstall --hook-type pre-push

The ``pre-commit`` command is a Python developer dependency so your
development virtualenv has to be active when you execute it. To execute the git
hooks themselves, the development virtualenv does not have to be active.

## Temporarily disable hooks

There are times you just want to quickly push your changes to remote, for
example to have a backup of your local commits. In those cases you don't want
Git to tell that it won't push your code. To let Git bypass any pre-push hook,
pass option ``--no-verify`` to ``git push``. Pass this option to ``git commit``
to let Git will bypass any commit hook.
