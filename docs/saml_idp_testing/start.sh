if [ -z "$1" ]
  then
    SPURL=http://172.17.0.1:8001
  else
    SPURL=$1
fi

docker pull kristophjunge/test-saml-idp

docker run --name=saml-idp-blue \
-p 8180:8080 \
-p 8543:8443 \
-e SIMPLESAMLPHP_SP_ENTITY_ID=$SPURL/saml/metadata/ \
-e SIMPLESAMLPHP_SP_ASSERTION_CONSUMER_SERVICE=$SPURL/saml/acs/ \
-e SIMPLESAMLPHP_SP_SINGLE_LOGOUT_SERVICE=$SPURL/saml/slo/ \
-v ~/concierge/docs/saml_idp_testing/test-saml-idp-user-blue.php:/var/www/simplesamlphp/config/authsources.php \
-v ~/concierge/docs/saml_idp_testing/test-saml-idp-config-blue.php:/var/www/simplesamlphp/config/config.php \
-v ~/concierge/docs/saml_idp_testing/gtk-dialog-authentication-blue.48x48.png:/var/www/simplesamlphp/www/resources/icons/experience/gtk-dialog-authentication.48x48.png \
-d kristophjunge/test-saml-idp

docker run --name=saml-idp-green \
-p 8280:8080 \
-p 8643:8443 \
-e SIMPLESAMLPHP_SP_ENTITY_ID=$SPURL/saml/metadata/ \
-e SIMPLESAMLPHP_SP_ASSERTION_CONSUMER_SERVICE=$SPURL/saml/acs/ \
-e SIMPLESAMLPHP_SP_SINGLE_LOGOUT_SERVICE=$SPURL/saml/slo/ \
-v ~/concierge/docs/saml_idp_testing/test-saml-idp-user-green.php:/var/www/simplesamlphp/config/authsources.php \
-v ~/concierge/docs/saml_idp_testing/test-saml-idp-config-green.php:/var/www/simplesamlphp/config/config.php \
-v ~/concierge/docs/saml_idp_testing/gtk-dialog-authentication-green.48x48.png:/var/www/simplesamlphp/www/resources/icons/experience/gtk-dialog-authentication.48x48.png \
-d kristophjunge/test-saml-idp

docker run --name=saml-idp-red \
-p 8380:8080 \
-p 8743:8443 \
-e SIMPLESAMLPHP_SP_ENTITY_ID=$SPURL/saml/metadata/ \
-e SIMPLESAMLPHP_SP_ASSERTION_CONSUMER_SERVICE=$SPURL/saml/acs/ \
-e SIMPLESAMLPHP_SP_SINGLE_LOGOUT_SERVICE=$SPURL/saml/slo/ \
-v ~/concierge/docs/saml_idp_testing/test-saml-idp-user-red.php:/var/www/simplesamlphp/config/authsources.php \
-v ~/concierge/docs/saml_idp_testing/test-saml-idp-config-red.php:/var/www/simplesamlphp/config/config.php \
-v ~/concierge/docs/saml_idp_testing/gtk-dialog-authentication-red.48x48.png:/var/www/simplesamlphp/www/resources/icons/experience/gtk-dialog-authentication.48x48.png \
-d kristophjunge/test-saml-idp

docker run --name=saml-idp-yellow \
-p 8480:8080 \
-p 8843:8443 \
-e SIMPLESAMLPHP_SP_ENTITY_ID=$SPURL/saml/metadata/ \
-e SIMPLESAMLPHP_SP_ASSERTION_CONSUMER_SERVICE=$SPURL/saml/acs/ \
-e SIMPLESAMLPHP_SP_SINGLE_LOGOUT_SERVICE=$SPURL/saml/slo/ \
-v ~/concierge/docs/saml_idp_testing/test-saml-idp-user-yellow.php:/var/www/simplesamlphp/config/authsources.php \
-v ~/concierge/docs/saml_idp_testing/gtk-dialog-authentication-yellow.48x48.png:/var/www/simplesamlphp/www/resources/icons/experience/gtk-dialog-authentication.48x48.png \
-d kristophjunge/test-saml-idp
