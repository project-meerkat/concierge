#!/usr/bin/env python
import os
import sys
import threading

# Fix for alpine segfault
threading.stack_size(2 * 1024 * 1024)

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "concierge.settings")
    try:
        from django.core.management import execute_from_command_line
    except ImportError:
        # The above import may fail for some other reason. Ensure that the
        # issue is really that Django is missing to avoid masking other
        # exceptions on Python 2.
        try:
            import django  # noqa: F401 'django' imported but unused
        except ImportError:
            msg = (
                "Couldn't import Django. Are you sure it's installed and "
                "available on your PYTHONPATH environment variable? Did you "
                "forget to activate a virtual environment?"
            )
            raise ImportError(msg)
        raise
    execute_from_command_line(sys.argv)
