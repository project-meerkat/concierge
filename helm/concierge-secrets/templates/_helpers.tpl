{{- define "concierge.name" -}}
{{- .Release.Name | trunc 63  -}}
{{- end -}}

{{- define "concierge.tlsSecretName" -}}
{{- printf "tls-%s" ((include "concierge.name" .)) -}}
{{- end -}}

{{- define "concierge.samlSecretName" -}}
{{- printf "%s-saml" ((include "concierge.name" .)) -}}
{{- end -}}

{{- define "concierge.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "concierge.labels" -}}
helm.sh/chart: {{ include "concierge.chart" . }}
{{ include "concierge.selectorLabels" . }}
{{- end -}}

{{- define "concierge.selectorLabels" -}}
app.kubernetes.io/name: {{ include "concierge.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}
