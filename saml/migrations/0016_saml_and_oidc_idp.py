# Generated by Django 2.2.12 on 2021-05-26 14:44

import django.db.models.deletion
from django.db import migrations, models
from django.utils import timezone

import saml.helpers


def update_saml_idp(apps, schema_editor):
    OldIdentityProvider = apps.get_model("saml", "OldIdentityProvider")
    SamlIdentityProvider = apps.get_model("saml", "SamlIdentityProvider")

    for old_idp in OldIdentityProvider.objects.all():
        SamlIdentityProvider.objects.create(
            id=old_idp.pk,  # ids need to stay the same for the foreign key links
            shortname=old_idp.shortname,
            displayname=old_idp.displayname,
            last_modified=old_idp.last_modified,
            metadata_url=old_idp.metadata_url,
            metadata_filename=old_idp.metadata_filename,
            entityId=old_idp.entityId,
            perform_slo=old_idp.perform_slo,
            connect_automatically=old_idp.connect_automatically,
            ssoId=old_idp.ssoId,
            sloId=old_idp.sloId,
            signing_x509cert1=old_idp.signing_x509cert1,
            signing_x509cert2=old_idp.signing_x509cert2,
            encryption_x509cert1=old_idp.encryption_x509cert1,
            encryption_x509cert2=old_idp.encryption_x509cert2,
        )


def reverse_update_saml_idp(apps, schema_editor):
    OldIdentityProvider = apps.get_model("saml", "OldIdentityProvider")
    SamlIdentityProvider = apps.get_model("saml", "SamlIdentityProvider")
    ExternalId = apps.get_model("saml", "ExternalId")

    saml_idps = SamlIdentityProvider.objects.all()

    for saml_idp in saml_idps:
        OldIdentityProvider.objects.create(
            id=saml_idp.pk,  # ids need to stay the same for the foreign key links
            shortname=saml_idp.shortname,
            displayname=saml_idp.displayname,
            last_modified=saml_idp.last_modified,
            metadata_url=saml_idp.metadata_url,
            metadata_filename=saml_idp.metadata_filename,
            entityId=saml_idp.entityId,
            perform_slo=saml_idp.perform_slo,
            connect_automatically=saml_idp.connect_automatically,
            ssoId=saml_idp.ssoId,
            sloId=saml_idp.sloId,
            signing_x509cert1=saml_idp.signing_x509cert1,
            signing_x509cert2=saml_idp.signing_x509cert2,
            encryption_x509cert1=saml_idp.encryption_x509cert1,
            encryption_x509cert2=saml_idp.encryption_x509cert2,
        )

    oidc_ids = [externalid for externalid in ExternalId.objects.all() if externalid.identityproviderid not in saml_idps]

    for externalid in oidc_ids:
        externalid.delete()


class Migration(migrations.Migration):
    dependencies = [
        ("saml", "0015_identityprovider_connect_automatically"),
    ]

    operations = [
        migrations.RenameModel("IdentityProvider", "OldIdentityProvider"),
        migrations.CreateModel(
            name="IdentityProvider",
            fields=[
                (
                    "id",
                    models.AutoField(
                        primary_key=True,
                        auto_created=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("shortname", models.SlugField(unique=True)),
                ("displayname", models.CharField(max_length=100, null=False)),
                ("last_modified", models.DateTimeField(default=timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name="OidcIdentityProvider",
            fields=[
                (
                    "identityprovider_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="saml.IdentityProvider",
                    ),
                ),
                ("client_id", models.CharField(max_length=256)),
                ("client_secret", models.CharField(max_length=256)),
                ("token_endpoint", models.URLField(max_length=256)),
                ("authorize_endpoint", models.URLField(max_length=256)),
                ("jwks_endpoint", models.URLField(max_length=256)),
                ("end_session_endpoint", models.URLField(blank=True, max_length=256)),
                ("sign_algo", models.CharField(max_length=20)),
            ],
            bases=("saml.identityprovider",),
        ),
        migrations.CreateModel(
            name="SamlIdentityProvider",
            fields=[
                (
                    "identityprovider_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="saml.IdentityProvider",
                    ),
                ),
                ("metadata_url", models.URLField(blank=True, max_length=256)),
                (
                    "metadata_filename",
                    models.FileField(blank=True, upload_to=saml.helpers.unique_idp_metadata_filepath),
                ),
                ("entityId", models.URLField(max_length=256)),
                ("perform_slo", models.BooleanField(default=True)),
                ("connect_automatically", models.BooleanField(default=False)),
                ("ssoId", models.URLField(blank=True, max_length=256)),
                ("sloId", models.URLField(blank=True, max_length=256)),
                ("signing_x509cert1", models.TextField(blank=True)),
                ("signing_x509cert2", models.TextField(blank=True)),
                ("encryption_x509cert1", models.TextField(blank=True)),
                ("encryption_x509cert2", models.TextField(blank=True)),
            ],
            bases=("saml.identityprovider",),
        ),
        migrations.RunPython(update_saml_idp, migrations.RunPython.noop),
        migrations.AlterField(
            model_name="IdpEmailDomain",
            name="identityprovider",
            field=models.ForeignKey(
                db_column="IdentityProvider.shortname",
                on_delete=models.CASCADE,
                db_index=True,
                to="saml.IdentityProvider",
            ),
        ),
        migrations.AlterField(
            model_name="ExternalId",
            name="identityproviderid",
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to="saml.IdentityProvider"),
        ),
        migrations.RunPython(migrations.RunPython.noop, reverse_update_saml_idp),
        migrations.DeleteModel("OldIdentityProvider"),
    ]
