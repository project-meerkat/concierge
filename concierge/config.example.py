# noqa: N999
import os
from urllib.parse import urljoin

from django.utils.translation import gettext_lazy as _

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "HWZUzWbMVAnzdwJxeYnACTazALmQ8b"

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

EXTERNAL_HOST = os.getenv("EXTERNAL_HOST")
"""
If server is behind proxys or balancers use the HTTP_X_FORWARDED fields
Check USE_X_FORWARDED_HOST and USE_X_FORWARDED_PORT in config.py
"""
USE_X_FORWARDED_HOST = True
USE_X_FORWARDED_PORT = True

# Include apps required before any other apps, like themes.
LOCAL_INSTALLED_APPS = [
    "themes.generic",
    # 'concierge_theme_gc',
    # 'concierge_theme_pleio',
]

LOCAL_AUTHENTICATION_BACKENDS = [
    # 'elgg.backends.ElggBackend'
]

LOCAL_AUTH_PASSWORD_VALIDATORS = []

# SECURITY WARNING: set this to True when running in production
SESSION_COOKIE_SECURE = False
SESSION_COOKIE_HTTPONLY = True
SESSION_COOKIE_SAMESITE = False

ALLOWED_HOSTS = []

# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(BASE_DIR, "db.sqlite3"),
    }
}

TIME_ZONE = "GMT"

LANGUAGE_CODE = "en"

LANGUAGES = [("en", _("English")), ("nl", _("Dutch")), ("fr", _("French"))]

DEFAULT_FROM_EMAIL = "concierge@hil.ton"

EMAIL_HOST = "mailcatcher"
EMAIL_HOST_USER = ""
EMAIL_HOST_PASSWORD = ""
EMAIL_PORT = 1025
EMAIL_USE_TLS = False

EMAIL_LOGO = "images/email-logo.png"

# This setting controls whether an image should be displayed when a user is
# also a member of other group(s) than "Any"
SHOW_GOVERNMENT_BADGE = True

ACCOUNT_ACTIVATION_DAYS = 7

# With the following test keys, you will always get No CAPTCHA and all
# verification requests will pass.
GOOGLE_RECAPTCHA_SITE_KEY = "6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"
GOOGLE_RECAPTCHA_SECRET_KEY = "6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe"

SAML2_SP = {
    "entityId": urljoin(os.getenv("EXTERNAL_HOST"), "saml/metadata/"),
    "assertionConsumerService": {
        "url": urljoin(os.getenv("EXTERNAL_HOST"), "saml/acs/"),
        "binding": "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST",
    },
    "singleLogoutService": {
        "url": urljoin(os.getenv("EXTERNAL_HOST"), "saml/slo/"),
        "binding": "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect",
    },
    "NameIDFormat": "urn:oasis:names:tc:SAML:2.0:nameid-format:persistent",
}

SAML2_SP_SECURITY = {
    "requestedAuthnContext": False,
    "authnRequestsSigned": False,
    "logoutRequestSigned": False,
    "logoutResponseSigned": False,
    "signatureAlgorithm": ("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"),
}

# Setting CELERY_ALWAYS_EAGER to "True"  will make task being executed locally
# in the client, not by a worker.
# Always use "False" in production environment.
CELERY_ALWAYS_EAGER = True
CELERY_BROKER_URL = os.getenv("CELERY_BROKER_URL")

# Stops mozilla-django-oidc classes from complaining, eventhough the settings are technically not used due to overwrites
OIDC_OP_TOKEN_ENDPOINT = ""
OIDC_OP_USER_ENDPOINT = ""
OIDC_RP_CLIENT_ID = ""
OIDC_RP_CLIENT_SECRET = ""
