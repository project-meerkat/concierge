"""
OIDC provider settings
"""

from django.utils.translation import gettext_lazy as _
from oidc_provider.lib.claims import ScopeClaims
from two_factor.utils import default_device

from core.helpers import is_admin, is_government


def userinfo(claims, user):
    """
    Populate claims dict.
    """
    claims["name"] = user.name
    claims["email"] = user.email
    if user.avatar:
        claims["picture"] = user.picture

    return claims


class CustomScopeClaims(ScopeClaims):
    def __init__(self, token):
        super().__init__(token)

        self._id_token = token.id_token or {}

    """
    Change description of profile scope.
    """
    info_profile = (
        _("Basic profile"),
        _("Access to your name and avatar."),
    )

    info_email = (
        _("Email address"),
        _("Access to your email address."),
    )

    def scope_profile(self):
        # self.user - Django user instance.
        # self.userinfo - Dict returned by OIDC_USERINFO function.
        # self.scopes - List of scopes requested.
        # self.client - Client requesting this claims.

        has_2fa_enabled = (
            default_device(self.user) is not None
            or self.user.session.get("via_sso", False)
            or self._id_token.get("has_2fa_enabled", False)
        )

        sso = list(self.user.externalid_set.values_list("identityproviderid__shortname", flat=True))
        dic = {
            "is_admin": is_admin(self.user.email),
            "is_government": is_government(self.user.email),
            "has_2fa_enabled": has_2fa_enabled,
            "sso": sso,
        }

        return dic
