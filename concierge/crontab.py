from celery.schedules import crontab

beat_schedule = {
    "user_cleanup": {
        "task": "core.tasks.cleanup_users",
        "schedule": crontab(minute=0, hour=5),
    },
    "daily_cleanup": {
        "task": "core.tasks.cleanup_display_name_history",
        "schedule": crontab(minute=0, hour=6),
    },
}
