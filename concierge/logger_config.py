import uuid
from datetime import datetime


def add_log_uuid(record):
    log_uuid = str(uuid.uuid4())
    record.log_uuid = log_uuid
    try:
        record.request.session["log_uuid"] = log_uuid
    except AttributeError:
        pass

    return True


def add_log_datetime(record):
    log_datetime = str(datetime.now())
    record.log_datetime = log_datetime
    try:
        record.request.session["log_datetime"] = log_datetime
    except AttributeError:
        pass

    return True
