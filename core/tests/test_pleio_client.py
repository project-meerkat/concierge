from unittest import mock

from mixer.backend.django import mixer

from core.models import OriginSite
from core.pleio import PleioClient
from core.tests import TestCase


class TestRefreshSiteFaviconTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.site = mixer.blend(OriginSite, origin_site_api_token="demo")
        self.client = PleioClient(self.site)

    def test_add_checksum(self):
        result = self.client.add_checksum({"Foo": "Bar"})
        self.assertIn("timestamp", result)
        self.assertIn("checksum", result)
        self.assertEqual(result["Foo"], "Bar")

    def test_checksum_value(self):
        result = self.client.add_checksum({"Foo": "Bar", "Koe": "Kaa", "timestamp": "baz"})
        self.assertEqual(
            result,
            {
                "Foo": "Bar",
                "Koe": "Kaa",
                "timestamp": "baz",
                "checksum": "d3cd9f69a887",
            },
        )

    @mock.patch("core.pleio.requests.post")
    @mock.patch("core.pleio.PleioClient.add_checksum")
    def test_post(self, mocked_add_checksum, mocked_post):
        mocked_post.return_value = mock.MagicMock()
        mocked_add_checksum.return_value = mock.MagicMock()

        result = self.client.post("path/")

        self.assertEqual(result, mocked_post.return_value)
        self.assertEqual(mocked_add_checksum.call_count, 1)
        self.assertEqual(mocked_post.call_args.args, (self.site.url("path/"),))
        self.assertEqual(mocked_post.call_args.kwargs, {"data": mocked_add_checksum.return_value})

    @mock.patch("core.pleio.requests.get")
    @mock.patch("core.pleio.PleioClient.add_checksum")
    def test_get(self, mocked_add_checksum, mocked_get):
        mocked_get.return_value = mock.MagicMock()
        mocked_add_checksum.return_value = mock.MagicMock()

        result = self.client.get("path/")

        self.assertEqual(result, mocked_get.return_value)
        self.assertEqual(mocked_add_checksum.call_count, 1)
        self.assertEqual(mocked_get.call_args.args, (self.site.url("path/"),))
        self.assertEqual(mocked_get.call_args.kwargs, {"data": mocked_add_checksum.return_value})
