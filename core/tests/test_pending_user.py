from django.utils.timezone import localtime, timedelta
from mixer.backend.django import mixer

from core.models import PendingUser, User
from core.tests import TestCase


class TestPendingUserTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.user: User = mixer.blend(User, is_active=False)
        self.pending_user: PendingUser = mixer.blend(PendingUser, user=self.user)

    def test_activate_user(self):
        # Given
        self.assertFalse(self.user.is_active)

        # When
        self.pending_user.activate_user()
        self.user.refresh_from_db()

        # Then
        self.assertTrue(self.user.is_active)


class TestPendingUserQuerySetTestCase(TestCase):
    def setUp(self):
        super().setUp()

        self.EXPIRED = localtime() - timedelta(hours=25)
        self.user: User = mixer.blend(User, is_active=False)
        self.pending_user: PendingUser = mixer.blend(PendingUser, user=self.user, created_at=self.EXPIRED)
        self.pending_user.created_at = self.EXPIRED
        self.pending_user.save()

    def test_expired_pending_user(self):
        with self.assertRaises(PendingUser.DoesNotExist):
            pending_user = PendingUser.objects.get(pk=self.pending_user.pk)
            pending_user.assert_not_due()

    def test_filter_expired_records(self):
        new_record = PendingUser.objects.create(user=mixer.blend(User))
        self.assertEqual([*PendingUser.objects.filter_expired_pending_users()], [self.pending_user])
        self.assertEqual([*PendingUser.objects.filter_valid_pending_users()], [new_record])


class TestPendingUserManagerTestCase(TestCase):
    def setUp(self):
        super().setUp()

        self.expired = self.create_pending_user(created_at=localtime() - timedelta(hours=25))
        self.valid = self.create_pending_user(created_at=localtime() - timedelta(hours=5))
        self.active = self.create_pending_user()

    @staticmethod
    def create_pending_user(**kwargs) -> PendingUser:
        if "user" not in kwargs:
            kwargs.setdefault("user", mixer.blend(User, is_active=False))
        pending_user = mixer.blend(PendingUser, **kwargs)
        if "created_at" in kwargs:
            pending_user.created_at = kwargs["created_at"]
            pending_user.save()
        return pending_user

    def test_cleanup(self):
        PendingUser.objects.cleanup(user=self.active.user)
        records = PendingUser.objects.all()

        self.assertNotIn(self.active, records)
        self.assertIn(self.valid, records)
        self.assertNotIn(self.expired, records)
