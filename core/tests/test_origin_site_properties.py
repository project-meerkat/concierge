from django.core.files.base import ContentFile
from mixer.backend.django import mixer

from core.models import OriginSite
from core.tests import ImageTestCase, load_asset


class TestOriginSitePropertiesTestCase(ImageTestCase):
    def setUp(self):
        super().setUp()

        self.site: OriginSite = mixer.blend(OriginSite)

    def tearDown(self):
        OriginSite.objects.all().delete()
        super().tearDown()

    def test_remove_origin_site(self):
        # Given
        path = self.site.origin_site_favicon.path

        # When
        self.site.delete()

        # Then
        self.assert_not_exists(path)

    def test_remove_favicon(self):
        # Given
        path = self.site.origin_site_favicon.path

        # When
        self.site.origin_site_favicon.name = ""
        self.site.save()

        # Then
        self.assert_not_exists(path)

    def test_overwrite_favicon(self):
        # given
        path = self.site.origin_site_favicon.path

        # when
        self.site.origin_site_favicon = ContentFile(*load_asset("favicon.png"))
        self.site.save()

        # Then
        self.assert_not_exists(path)
