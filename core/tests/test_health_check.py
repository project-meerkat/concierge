from core.tests import TestCase


class TestHealthCheck(TestCase):
    def setUp(self):
        super().setUp()
        self.maxDiff = None
        self.override_settings(HEALTHCHECK_IP='["127.0.0.1"]')

    def test_health_check_endpoint(self):
        response = self.client.get("/ht/", HTTP_ACCEPT="application/json")
        self.assertEqual(response.headers["Content-Type"], "application/json")
        payload = response.json()

        self.assert_dict_contains_subset(
            payload,
            {
                "Cache backend: default": "working",
                "DatabaseBackend": "working",
                "DefaultFileStorageHealthCheck": "working",
                "MigrationsHealthCheck": "working",
            },
        )

        # These services don't run in the unit-test environment.
        self.assertIn("CeleryPingHealthCheck", payload)
        self.assertIn("RabbitMQHealthCheck", payload)

    def test_health_check_endpoint_basic(self):
        response = self.client.get("/ht/basic/", HTTP_ACCEPT="application/json")
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 200)

        self.assert_dict_contains_subset(
            response.json(),
            {
                "Cache backend: default": "working",
            },
        )

    def test_health_check_endpoint_extended(self):
        response = self.client.get("/ht/extended/", HTTP_ACCEPT="application/json")
        self.assertEqual(response.headers["Content-Type"], "application/json")
        payload = response.json()

        self.assert_dict_contains_subset(
            payload,
            {
                "Cache backend: default": "working",
                "DatabaseBackend": "working",
                "DefaultFileStorageHealthCheck": "working",
                "MigrationsHealthCheck": "working",
            },
        )
        # These services don't run in the unit-test environment.
        self.assertIn("RabbitMQHealthCheck", payload)
