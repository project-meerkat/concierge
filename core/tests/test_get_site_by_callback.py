import urllib.parse

from mixer.backend.django import mixer

from core.helpers import get_site_by_callback
from core.models import OriginSite
from core.tests import TestCase


class TestGetSiteByCallbackTestCase(TestCase):
    def setUp(self):
        super().setUp()

        self.site = mixer.blend(OriginSite, origin_site_url="http://www.pleio-test.nl:8000")
        self.site2 = mixer.blend(OriginSite, origin_site_url="http://www.pleio-test.nl")
        self.url = "/openid/authorize/?" + urllib.parse.urlencode(
            {
                "response_type": "code",
                "scope": "openid profile",
                "redirect_uri": urllib.parse.urljoin(self.site.origin_site_url, "/oidc/callback/"),
            }
        )

    def test_get_site_by_callback(self):
        maybe_site = get_site_by_callback(self.url)
        self.assertEqual(maybe_site, self.site)
        self.assertNotEqual(maybe_site, self.site2)
