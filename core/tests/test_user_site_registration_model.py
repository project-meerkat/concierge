from unittest import mock

from mixer.backend.django import mixer

from core.models import OriginSite, User, UserSiteRegistration
from core.tests import TestCase


class TestUserSiteRegistrationModel(TestCase):
    def setUp(self):
        super().setUp()

        self.user = mixer.blend(User)
        self.origin_site = mixer.blend(OriginSite)

    def assert_is_local_record(self, record):
        self.assertEqual(record.user, self.user)
        self.assertEqual(record.origin_site, self.origin_site)

    @mock.patch("core.models.get_similar_display_name_accounts")
    @mock.patch("core.models.DisplayNameHistoryManager.add_to_history")
    def test_existing_user_registration(self, add_to_history, get_similar_display_name_accounts):
        mixer.blend(UserSiteRegistration, user=self.user, origin_site=self.origin_site)

        maybe_new_record, is_new = UserSiteRegistration.objects.get_or_create(
            user=self.user, origin_site=self.origin_site
        )

        self.assertFalse(is_new)
        self.assert_is_local_record(maybe_new_record)
        self.assertFalse(get_similar_display_name_accounts.called)
        self.assertFalse(add_to_history.called)

    @mock.patch("core.models.get_similar_display_name_accounts")
    @mock.patch("core.models.DisplayNameHistoryManager.add_to_history")
    def test_unique_user_registration(self, add_to_history, get_similar_display_name_accounts):
        get_similar_display_name_accounts.return_value = UserSiteRegistration.objects.none()

        maybe_new_record, is_new = UserSiteRegistration.objects.get_or_create(
            user=self.user, origin_site=self.origin_site
        )

        self.assertTrue(is_new)
        self.assert_is_local_record(maybe_new_record)
        self.assertTrue(get_similar_display_name_accounts.called)
        self.assertFalse(add_to_history.called)

    @mock.patch("core.models.get_similar_display_name_accounts")
    @mock.patch("core.models.DisplayNameHistoryManager.add_to_history")
    def test_matching_user_registration(self, add_to_history, get_similar_display_name_accounts):
        other_registration = mixer.blend(UserSiteRegistration, user=mixer.blend(User), origin_site=self.origin_site)

        get_similar_display_name_accounts.return_value = UserSiteRegistration.objects.filter(id=other_registration.id)

        maybe_new_record, is_new = UserSiteRegistration.objects.get_or_create(
            user=self.user, origin_site=self.origin_site
        )

        self.assertTrue(is_new)
        self.assert_is_local_record(maybe_new_record)
        self.assertTrue(get_similar_display_name_accounts.called)
        self.assertTrue(add_to_history.called)
