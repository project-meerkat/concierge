from constance import config as constance_config

from core.models import ValidCountry
from core.tests import TestCase

"""
overheid.nl: 62.112.233.85
    >>> g.country('62.112.233.85')
    {'country_code': 'NL', 'country_name': 'Netherlands'}

nytimes.com: 151.101.193.164
    >>> g.country('151.101.193.164')
    {'country_code': 'US', 'country_name': 'United States'}

pravda.ru: 185.201.54.50
    >>> g.country('185.201.54.50')
    {'country_code': 'RU', 'country_name': 'Russia'}

whitehouse.gov: 88.221.71.111
    >>> g.country('88.221.71.111')
    {'country_code': None, 'country_name': None}

>>> g.country('10.0.0.1')
    geoip2.errors.AddressNotFoundError:
    The address 10.0.0.1 is not in the database.
"""


class ValidCountryTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.country = ValidCountry.objects.create(country_code="NL", country_name="Netherlands", is_valid_country=True)

        self.country = ValidCountry.objects.create(
            country_code="US", country_name="United States", is_valid_country=False
        )

    def test_valid_country_no_limitation(self):
        """Test no country limitation"""
        constance_config.REGISTRATION_LIMITED_TO_VALID_COUNTRIES = False

        ip = "62.112.233.85"  # country_code=NL
        result = ValidCountry.valid_country(ip)
        # No testing required, all countries are valid
        self.assertIs(result, True)

        ip = "151.101.193.164"  # country_code=US
        result = ValidCountry.valid_country(ip)
        # No testing required, all countries are valid
        self.assertIs(result, True)

        ip = "185.201.54.50"  # country_code=RU
        result = ValidCountry.valid_country(ip)
        # No testing required, all countries are valid
        self.assertIs(result, True)

        ip = "10.0.0.1"  # not in GeoIP2 datbase
        result = ValidCountry.valid_country(ip)
        # No testing required, all countries are valid
        self.assertIs(result, True)

        ip = None  # no IP address
        result = ValidCountry.valid_country(ip)
        # No testing required, all countries are valid
        self.assertIs(result, True)

    def test_valid_country_limitation_unknown_not_allowed(self):
        """Test only valid countries allowed"""
        constance_config.REGISTRATION_LIMITED_TO_VALID_COUNTRIES = True
        constance_config.REGISTRATION_ALLOWED_FOR_UNKNOWN_COUNTRIES = False

        ip = "62.112.233.85"  # country_code=NL
        result = ValidCountry.valid_country(ip)
        # NL is valid country
        self.assertIs(result, True)

        ip = "151.101.193.164"  # country_code=US
        result = ValidCountry.valid_country(ip)
        # US is not a valid country
        self.assertIs(result, False)

        ip = "185.201.54.50"  # country_code=RU
        result = ValidCountry.valid_country(ip)
        # RU is not in ValidCountry, unknown country not allowed
        self.assertIs(result, False)

        ip = "10.0.0.1"  # not in GeoIP2 datbase
        result = ValidCountry.valid_country(ip)
        # IP address not in GEOIP2 database, unknown country not allowed
        self.assertIs(result, False)

        ip = None  # no IP address
        result = ValidCountry.valid_country(ip)
        # No IP address, unknown country not allowed
        self.assertIs(result, False)

    def test_valid_country_limitation_unknown_allowed(self):
        """Test both valid and unknown countries are allowed"""
        constance_config.REGISTRATION_LIMITED_TO_VALID_COUNTRIES = True
        constance_config.REGISTRATION_ALLOWED_FOR_UNKNOWN_COUNTRIES = True

        ip = "62.112.233.85"  # country_code=NL
        result = ValidCountry.valid_country(ip)
        # NL is valid country
        self.assertIs(result, True)

        ip = "151.101.193.164"  # country_code=US
        result = ValidCountry.valid_country(ip)
        # US is not a valid country
        self.assertIs(result, False)

        ip = "185.201.54.50"  # country_code=RU
        result = ValidCountry.valid_country(ip)
        # RU is not in ValidCountry, unknown country allowed
        self.assertIs(result, True)

        ip = "88.221.71.111"  # country_code=None
        result = ValidCountry.valid_country(ip)
        # No country code, unknown country allowed
        self.assertIs(result, True)

        ip = "10.0.0.1"  # not in GeoIP2 datbase
        result = ValidCountry.valid_country(ip)
        # IP address not in GEOIP2 database, unknown country allowed
        self.assertIs(result, True)

        ip = None  # no IP address
        result = ValidCountry.valid_country(ip)
        # No IP address, unknown country allowed
        self.assertIs(result, True)
