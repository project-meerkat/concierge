from django.contrib.auth import get_user_model
from oauth2_provider.exceptions import OAuthToolkitError
from oauth2_provider.models import Application

from core.tests import TestCase


class TestOAuth2ProviderWildcards(TestCase):
    def setUp(self):
        super().setUp()

        user_model = get_user_model()
        self.user = user_model.objects.create_user("testuser", "test@example.com", "testpass")
        self.user.accepted_terms = True
        self.user.is_active = True
        self.user.save()

        self.application = Application.objects.create(
            name="Test Application",
            redirect_uris="https://*.example.com/*",
            user=self.user,
            client_type=Application.CLIENT_CONFIDENTIAL,
            authorization_grant_type=Application.GRANT_AUTHORIZATION_CODE,
        )

    def test_oauth_authorization_with_wildcard_redirect(self):
        auth_url = "/oauth/v2/authorize"
        auth_params = {
            "response_type": "code",
            "client_id": self.application.client_id,
            "redirect_uri": "https://app.example.com/callback",
            "scope": "read write",
        }

        # Ensure the user is logged in
        login_successful = self.client.login(username="testuser", password="testpass")
        self.assertTrue(login_successful, "Login failed")

        try:
            # First, make a GET request to the authorization page
            response = self.client.get(auth_url, auth_params)
            self.assertEqual(response.status_code, 200, "Failed to load authorization page")

            # Now make the POST request to simulate user authorization
            post_data = {
                "client_id": self.application.client_id,
                "response_type": "code",
                "state": "random_state_string",
                "allow": "Authorize",
                "redirect_uri": "https://app.example.com/callback",
                "scope": "read write",
            }
            response = self.client.post(auth_url, post_data, follow=False)

            # Check if the response is a redirect (302) to the callback URL
            self.assertEqual(response.status_code, 302, "Expected a redirect response")
            self.assertTrue(
                response["Location"].startswith("https://app.example.com/callback?code="),
                "Redirect to callback URL failed",
            )
        except OAuthToolkitError as e:
            self.fail(f"OAuth Toolkit Error: {str(e)}")

    def test_oauth_authorization_with_invalid_redirect(self):
        auth_url = "/oauth/v2/authorize"
        auth_params = {
            "response_type": "code",
            "client_id": self.application.client_id,
            "redirect_uri": "https://malicious.com/callback",
        }

        self.client.login(username="testuser", password="testpass")

        try:
            response = self.client.get(auth_url, auth_params)
            self.assertEqual(response.status_code, 400)
        except OAuthToolkitError as e:
            self.fail(f"OAuth Toolkit Error: {str(e)}")

    def test_client_id_exists(self):
        # Add this test to verify the client ID
        self.assertTrue(Application.objects.filter(client_id=self.application.client_id).exists())
