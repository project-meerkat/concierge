from django.core.files.base import ContentFile
from mixer.backend.django import mixer

from core.avatar_processing import Avatar
from core.models import User
from core.tests import ImageTestCase, load_asset


class TestFaviconHandlerTestCase(ImageTestCase):
    def setUp(self):
        super().setUp()

        self.COMMON_AVATAR = "avatar-with-exif.jpg"
        self.SMALL_AVATAR = "avatar-55px.jpg"
        self.BIG_AVATAR = "avatar-999px.jpg"

        self.user = mixer.blend(User)

    def tearDown(self):
        avatar = Avatar(self.user)
        avatar.previous_name = self.user.avatar.name
        avatar.cleanup_avatar()

        super().tearDown()

    def use_avatar(self, asset):
        self.user.avatar = ContentFile(*load_asset(asset))
        self.user.save()
        avatar = Avatar(self.user)
        avatar.create_thumbnails()
        return avatar

    def test_remove_exif(self):
        # Given.
        self.use_avatar(self.COMMON_AVATAR)
        self.assert_exif(self.user.avatar.name)
        avatar = Avatar(self.user)

        # When.
        avatar.strip_exif()

        # Then.
        self.assert_no_exif(self.user.avatar.name)

    def test_create_thumbnails(self):
        avatar = self.use_avatar(self.COMMON_AVATAR)

        self.assert_exists(avatar.thumbnail_filename("large"))
        self.assert_image_size_max(avatar.thumbnail_filename("large"), (80, 80))
        self.assert_exists(avatar.thumbnail_filename("medium"))
        self.assert_image_size_max(avatar.thumbnail_filename("medium"), (40, 40))
        self.assert_exists(avatar.thumbnail_filename("small"))
        self.assert_image_size_max(avatar.thumbnail_filename("small"), (32, 32))

    def test_create_oversized_thumbnails(self):
        avatar = self.use_avatar(self.SMALL_AVATAR)

        self.assert_exists(avatar.thumbnail_filename("large"))
        self.assert_image_size_max(avatar.thumbnail_filename("large"), (55, 55))

    def test_create_bigger_thumbnails(self):
        avatar = self.use_avatar(self.BIG_AVATAR)
        self.assert_not_exists(avatar.thumbnail_filename(600))

        avatar.thumbnail_size_path(600)

        self.assert_exists(avatar.thumbnail_filename(600))
        self.assert_image_size_max(avatar.thumbnail_filename(600), (600, 600))

    def test_cleanup(self):
        # given, a user move to a no-avatar state
        self.use_avatar(self.COMMON_AVATAR)
        self.user.stored_avatar = self.user.avatar.name
        self.user.avatar = None
        avatar = Avatar(self.user)

        avatar.cleanup_avatar()
        self.assert_not_exists(self.user.stored_avatar)
        self.assert_not_exists(avatar.thumbnail_filename("large"))
        self.assert_not_exists(avatar.thumbnail_filename("medium"))
        self.assert_not_exists(avatar.thumbnail_filename("small"))

    def test_empty_avatar_file(self):
        self.user.avatar = ContentFile(b"", "empty.jpg")
        self.user.save()

        with self.assert_not_raises_exception():
            avatar = Avatar(self.user)
            avatar.create_thumbnails()
