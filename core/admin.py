import re

from django.contrib import admin, messages
from django.contrib.admin import DateFieldListFilter, helpers
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError
from django.http import HttpResponseRedirect
from django.template.response import TemplateResponse
from django.urls import path, reverse
from django.utils import timezone
from django.utils.html import escape, format_html
from django.utils.translation import gettext_lazy as _
from django_otp.plugins.otp_totp.admin import TOTPDeviceAdmin
from django_otp.plugins.otp_totp.models import TOTPDevice

from reporting.models import Report
from saml.models import ExternalId

from .admin_views import DisplayNameChangeInspectView
from .filters import SingleTextInputFilter
from .models import (
    ClientWhitelist,
    DisplayNameHistory,
    EventLog,
    GovernmentDomain,
    OriginSite,
    Page,
    PendingUser,
    PreviousLogin,
    User,
    UserSiteRegistration,
    ValidCountry,
)


class ExportCsvMixin:
    def export_as_csv(self, request, queryset):
        from reporting.tasks import create_report

        report = Report.objects.create_report(self.model, queryset, request.user)
        create_report.delay(report.guid)

        messages.info(
            request,
            "Scheduled export selected records. You will receive a notification when the report is ready for download.",
        )
        return HttpResponseRedirect(request.path)

    export_as_csv.short_description = _("Export Selected")


class UserIdFilter(SingleTextInputFilter):
    title = _("User Id")
    parameter_name = "userid"

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(userid__iexact=self.value())


class EmailFilter(SingleTextInputFilter):
    title = _("Email address")
    parameter_name = "email"

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(email__iexact=self.value())


class IPFilter(SingleTextInputFilter):
    title = _("IP address")
    parameter_name = "ip"

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(ip__iexact=self.value())


class ExternalIdInline(admin.TabularInline):
    model = ExternalId
    extra = 0


class PreviousLoginInline(admin.TabularInline):
    model = PreviousLogin
    extra = 0
    ordering = ("-last_login_date",)
    readonly_fields = (
        "ip",
        "user_agent",
        "city",
        "country",
        "latitude",
        "longitude",
        "last_login_date",
    )


class MyUserCreationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ("email",)

    def __init__(self, *args, **kwargs):
        super(MyUserCreationForm, self).__init__(*args, **kwargs)
        self.fields["password"].required = False
        self.fields["password1"].required = False
        self.fields["password2"].required = False

    def clean_password(self):
        return None

    def clean_avatar(self):
        avatar = self.cleaned_data.get("avatar")
        if not avatar:
            return

        mimetype = avatar.content_type.split("/")[0]
        if mimetype.startswith("image"):
            return avatar
        msg = "The uploaded file is not an image."
        raise ValidationError(msg)

    def save(self, commit=True):
        avatar = self.cleaned_data.get("avatar")
        user = super().save(commit=False)
        user.avatar = None
        if avatar:
            user.save()
            user.avatar = avatar

        if commit:
            user.save()

        return user


class MyUserChangeForm(UserChangeForm):
    class Meta:
        model = User
        fields = ("name", "email", "password")


class MyUserAdmin(UserAdmin):
    add_form_template = "admin/auth/user/add_form.html"
    change_user_password_template = None
    form = MyUserChangeForm
    add_form = MyUserCreationForm


class CustomDateFilter(DateFieldListFilter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        now = timezone.now()
        early_this_month = timezone.datetime(year=now.year, month=now.month, day=1)
        last_month = early_this_month - timezone.timedelta(days=1)
        early_last_month = timezone.datetime(year=last_month.year, month=last_month.month, day=1)

        self.links = (
            *self.links,
            (
                early_last_month.strftime("%B %Y"),
                {
                    self.lookup_kwarg_since: str(early_last_month),
                    self.lookup_kwarg_until: str(early_this_month),
                },
            ),
        )


class CustomUserAdmin(MyUserAdmin, ExportCsvMixin):
    search_fields = admin.ModelAdmin.search_fields + ("id", "username", "name", "email")
    list_filter = admin.ModelAdmin.list_filter + (
        "is_active",
        "is_superuser",
        "is_smoothoperator",
        "is_banned",
        ("time_created", CustomDateFilter),
    )
    list_display = (
        "id",
        "email",
        "name",
        "is_active",
    )
    list_display_links = ("id", "email", "name")
    filter_horizontal = ()
    ordering = ("-id",)
    actions = ["export_as_csv", "alter_mail_domain"]
    fieldsets = add_fieldsets = (
        (
            None,
            {
                "fields": (
                    "last_login",
                    "time_created",
                    "email",
                    "name",
                    "username",
                    "password",
                    "avatar",
                )
            },
        ),
        (
            "Settings",
            {
                "fields": (
                    "accepted_terms",
                    "is_active",
                    "is_superuser",
                    "is_smoothoperator",
                    "is_banned",
                )
            },
        ),
    )
    inlines = [
        ExternalIdInline,
        PreviousLoginInline,
    ]
    readonly_fields = ("username",)

    def alter_mail_domain(self, request, queryset):
        if request.POST.get("post"):
            new_domain = request.POST.get("new_domain", "")
            if not new_domain.startswith("@"):
                new_domain = "@" + new_domain

            for user in queryset:
                local_name = re.findall("[^@]*", user.email)[0]
                user.email = local_name + new_domain
                try:
                    user.save()
                except IntegrityError:
                    pass

            return None

        context = {
            **self.admin_site.each_context(request),
            "queryset": queryset,
            "action_checkbox_name": helpers.ACTION_CHECKBOX_NAME,
        }

        return TemplateResponse(request, "admin/core/user/alter_email_domain.html", context)


class CustomOriginSitesAdmin(admin.ModelAdmin):
    list_display = ("__str__", "origin_site_url")
    readonly_fields = (
        "origin_site_name",
        "origin_site_url",
        "origin_site_description",
        "origin_site_favicon",
        "origin_site_api_token",
    )

    search_fields = (
        "origin_site_name",
        "origin_site_url",
    )


class CustomEventLogAdmin(admin.ModelAdmin):
    actions = None
    search_fields = []
    readonly_fields = ("id", "userid", "email", "ip", "event_type", "event_time")
    ordering = ("-event_time",)
    list_filter = [
        UserIdFilter,
        EmailFilter,
        IPFilter,
        "event_type",
    ]
    list_display_links = None
    list_display = ("userid", "email", "ip", "event_type", "event_time")


class CustomValidCountryAdmin(admin.ModelAdmin):
    ordering = ("country_name",)
    list_filter = [
        "is_valid_country",
    ]
    search_fields = ["country_code", "country_name", "is_valid_country"]
    list_display = ("country_code", "country_name", "is_valid_country")


class CustomGovernmentDomainAdmin(admin.ModelAdmin):
    ordering = ("name",)
    search_fields = ["name", "email_domain"]
    list_display = ("name", "email_domain")


class CustomTOTPDeviceAdmin(TOTPDeviceAdmin):
    search_fields = ["user__email"]


class PendingUserAdmin(admin.ModelAdmin):
    ordering = ("-created_at",)
    search_fields = ("user__email", "user__name")
    list_display = ("__str__", "created_at")


class CustomUserSiteRegistrationAdmin(admin.ModelAdmin):
    list_display = ("__str__", "user_name", "user", "site_url", "created_at")
    search_fields = (
        "user__email",
        "user__name",
        "origin_site__origin_site_url",
        "origin_site__origin_site_name",
    )


class DisplayNameHistoryAdmin(admin.ModelAdmin):
    list_display = ("pk", "created_at", "changed_to", "user_email")
    search_fields = ("name", "original_name", "user__email")

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def get_urls(self):
        return [
            path(
                "<pk>/inspect",
                self.admin_site.admin_view(DisplayNameChangeInspectView.as_view()),
                name="display_name_change_inspect",
            ),
            *super().get_urls(),
        ]

    @staticmethod
    def changed_to(obj):
        url = reverse("admin:display_name_change_inspect", args=[obj.pk])
        suffix = " 👃" if obj.similar_accounts.exists() else ""
        return format_html(f'<a href="{url}" title="Inspect">{escape(str(obj))}{suffix}</a>')

    @staticmethod
    def user_email(obj):
        return obj.user.email


class CustomClientWhitelistAdmin(admin.ModelAdmin):
    ordering = ("client",)
    search_fields = ["client", "name", "email_domains"]
    list_display = ("name", "client", "email_domains")


# Register your models here.
admin.site.register(User, CustomUserAdmin)
admin.site.register(PendingUser, PendingUserAdmin)
admin.site.register(OriginSite, CustomOriginSitesAdmin)
admin.site.register(UserSiteRegistration, CustomUserSiteRegistrationAdmin)
admin.site.register(Page)
admin.site.register(EventLog, CustomEventLogAdmin)
admin.site.register(ValidCountry, CustomValidCountryAdmin)
admin.site.register(GovernmentDomain, CustomGovernmentDomainAdmin)
admin.site.unregister(TOTPDevice)
admin.site.register(TOTPDevice, CustomTOTPDeviceAdmin)
admin.site.register(DisplayNameHistory, DisplayNameHistoryAdmin)
admin.site.register(ClientWhitelist, CustomClientWhitelistAdmin)
