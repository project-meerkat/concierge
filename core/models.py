import logging
import os
import uuid
from urllib.parse import urljoin, urlparse

import kombu
from constance import config as constance_config
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.contrib.gis.geoip2 import GeoIP2
from django.core import signing
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_delete, pre_save
from django.dispatch import receiver
from django.templatetags.static import static
from django.urls import reverse
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.utils.timezone import get_current_timezone, localtime, timedelta
from django.utils.translation import get_language
from django.utils.translation import gettext_lazy as _

from .avatar_processing import Avatar
from .helpers import (
    get_site_by_callback,
    get_site_by_url,
    unique_avatar_filepath,
    unique_filepath,
)
from .login_session_helpers import get_city, get_country, get_device, get_lat_lon
from .mailers import send_suspicious_login_message

logger = logging.getLogger(__name__)


def _ban_users(records):
    try:
        from core.tasks import ban_user

        for r in records:
            ban_user.delay(*r, _("At the user's request"))
    except (ConnectionRefusedError, kombu.exceptions.OperationalError):
        pass


class UserQuerySet(models.QuerySet):
    def delete(self):
        records = []
        try:
            records = [(r.pk, r.email) for r in self]
            return super().delete()
        finally:
            _ban_users(records)

    def filter_expired_users(self):
        return self.filter(is_active=False, time_created__lt=timezone.now() - timedelta(days=60))


class UserManager(BaseUserManager):
    def get_queryset(self):
        return UserQuerySet(model=User, using=self._db)

    def create_user(self, email, name, password=None, accepted_terms=False):
        if not email:
            msg = "Users must have an email address"
            raise ValueError(msg)

        user = User.objects.filter(email__iexact=email, is_active=False).first()
        if not user:
            user = self.model(email=email.lower(), name=name)

        user.set_password(password)
        user.accepted_terms = accepted_terms

        user.save(using=self._db)
        return user

    def create_superuser(self, email, name, password):
        user = self.create_user(email=email.lower(), name=name, password=password)

        user.is_superuser = True
        user.is_active = True

        user.save(using=self._db)
        return user

    def filter_expired_users(self):
        return self.get_queryset().filter_expired_users()


class User(AbstractBaseUser):
    objects = UserManager()

    username = models.SlugField(unique=True)
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=255, unique=True)
    accepted_terms = models.BooleanField(default=False)
    avatar = models.ImageField(upload_to=unique_avatar_filepath, null=True, blank=True)
    new_email = models.CharField(max_length=255, null=True, blank=True, default=None)
    stored_avatar = models.CharField(max_length=255, null=True, blank=True)
    time_created = models.DateTimeField(default=timezone.now)

    is_active = models.BooleanField(default=False)
    # Stored in is_admin for legacy reasons.
    is_superuser = models.BooleanField(db_column="is_admin", default=False)
    is_smoothoperator = models.BooleanField(default=False)
    is_banned = models.BooleanField(default=False)

    session = {}

    __saved_avatar = None

    REQUIRED_FIELDS = ["name"]
    USERNAME_FIELD = "email"

    def __init__(self, *args, **kwargs):
        super(User, self).__init__(*args, **kwargs)
        if self.avatar:
            self.__saved_avatar = self.avatar

    def save(self, *args, **kwargs):
        self.email = self.email.lower()
        if not self.username:
            self.username = self.email
        super(User, self).save(*args, **kwargs)

    def process_avatar(self):
        avatar = Avatar(user=self)
        if not self.avatar.name:
            avatar.cleanup_avatar()
        elif avatar.is_changed():
            avatar.strip_exif()
            avatar.create_thumbnails()
            avatar.cleanup_avatar()

    def update_avatar_name(self):
        User.objects.filter(pk=self.pk).update(stored_avatar=self.avatar.name)

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    def get_full_name(self):
        return self.name

    def get_short_name(self):
        return self.name

    def change_email(self, activation_token):
        try:
            email = signing.loads(activation_token, max_age=settings.ACCOUNT_ACTIVATION_DAYS * 86400)

            if email is None:
                return None

            self = User.objects.get(new_email__iexact=email)

            # don't alter usernames of accounts that still use a separate username
            if self.username == self.email:
                self.username = self.new_email
            self.email = self.new_email
            self.new_email = None
            self.save()

            # Invalidate all PendingUser objects for this user
            PendingUser.objects.filter(user=self).delete()

            from core.tasks import profile_updated_signal

            profile_updated_signal.delay(self.id)

            return self

        except (signing.BadSignature, User.DoesNotExist):
            return None

    def check_users_previous_logins(self, request):
        # check whether user has previously logged in from this location and
        # using this device
        session = request.session

        try:
            login = self.previous_logins.get(ip=session.ip, user_agent=get_device(session.user_agent))
            login.update_previous_login(request)
            return True
        except PreviousLogin.DoesNotExist:
            PreviousLogin.add_known_login(request, self)
            if constance_config.SENDING_EMAIL_AFTER_SUSPICIOUS_LOGIN is True:
                send_suspicious_login_message(self, request)
            return False

    def delete(self, *args, **kwargs):
        record = self.pk, self.email
        super().delete(*args, **kwargs)
        _ban_users([record])

    @property
    def is_staff(self):
        return self.is_superuser

    @property
    def picture(self):
        try:
            if os.path.exists(self.avatar.path):
                avatar = Avatar(user=self)
                path = reverse("avatar", args=[self.id, avatar.crc_filename()])
                return urljoin(settings.EXTERNAL_HOST, path)
        except Exception:
            pass
        return None

    @property
    def avatar_url(self):
        if picture := self.picture:
            return picture
        return self.default_avatar_url()

    @property
    def original_avatar_url(self):
        if not self.avatar.name:
            return None
        return urljoin(settings.EXTERNAL_HOST, self.avatar.url)

    @staticmethod
    def default_avatar_url():
        return urljoin(settings.EXTERNAL_HOST, "/static/avatars/default/large.jpg")


class PendingUserQuerySet(models.QuerySet):
    @staticmethod
    def get_threshold():
        return localtime() - timedelta(hours=24)

    def filter_recent(self, minutes, **kwargs):
        return self.filter(created_at__gt=self.timedelta(minutes), **kwargs)

    def timedelta(self, minutes):
        return localtime() - timedelta(minutes=minutes)

    def expired_records(self):
        return Q(
            Q(
                type=PendingUser.PENDING_LOGIN,
                created_at__lt=self.timedelta(settings.LOGIN_EMAIL_EXPIRE_MINUTES),
            )
            | Q(
                type=PendingUser.PENDING_PWD_RESET,
                created_at__lt=self.timedelta(settings.LOGIN_EMAIL_EXPIRE_MINUTES),
            )
            | Q(
                type=PendingUser.PENDING_NEW_PROFILE,
                created_at__lt=self.timedelta(settings.NEW_PROFILE_EXPIRE_MINUTES),
            )
            | Q(
                type=PendingUser.PENDING_REGISTRATION,
                created_at__lt=self.timedelta(settings.REGISTRATION_EXPIRE_MINUTES),
            )
        )

    def filter_expired_pending_users(self):
        return self.filter(self.expired_records())

    def filter_valid_pending_users(self):
        return self.exclude(self.expired_records())


class PendingUserManager(models.Manager):
    def get_queryset(self):
        return PendingUserQuerySet(model=self.model, using=self._db)

    def get_valid(self, **kwargs):
        pass

    def filter_expired_pending_users(self):
        return self.get_queryset().filter_expired_pending_users()

    def filter_valid_pending_users(self):
        return self.get_queryset().filter_valid_pending_users()

    def cleanup(self, user: User):
        qs = self.get_queryset()
        qs = qs.filter(Q(user=user) | Q(created_at__lte=PendingUserQuerySet.get_threshold()))
        return qs.delete()

    def filter_recent(self, minutes, **kwargs):
        return self.get_queryset().filter_recent(minutes=minutes, **kwargs)


class PendingUser(models.Model):
    """
    Pending user authentication hatstand.
    """

    PENDING_REGISTRATION = "pending_registration"
    PENDING_LOGIN = "pending_login"
    PENDING_PWD_RESET = "pending_pwd_reset"
    PENDING_NEW_PROFILE = "pending_new_profile"

    TYPE_CHOICES = (
        (PENDING_REGISTRATION, _("Pending registration")),
        (PENDING_NEW_PROFILE, _("Pending new profile")),
        (PENDING_LOGIN, _("Pending login")),
        (PENDING_PWD_RESET, _("Pending password reset")),
    )

    objects = PendingUserManager()

    id = models.UUIDField(default=uuid.uuid4, primary_key=True)
    created_at = models.DateTimeField(default=localtime)

    user = models.ForeignKey("User", on_delete=models.CASCADE)
    next = models.TextField(null=True)
    type = models.CharField(max_length=128, choices=TYPE_CHOICES, default=PENDING_REGISTRATION)

    @property
    def guid(self):
        return str(self.pk)

    def activate_user(self):
        if self.user.is_active:
            return

        self.user.is_active = True
        self.user.save()

    def get_origin_site(self):
        return get_site_by_callback(self.next) or get_site_by_url(self.next)

    def get_expire_time(self):
        created = self.created_at.astimezone(get_current_timezone())

        if self.type == self.PENDING_LOGIN:
            return created + timedelta(minutes=settings.LOGIN_EMAIL_EXPIRE_MINUTES)

        elif self.type == self.PENDING_PWD_RESET:
            return created + timedelta(minutes=settings.PASSWORD_RESET_EMAIL_EXPIRE_MINUTES)

        elif self.type == self.PENDING_NEW_PROFILE:
            return created + timedelta(minutes=settings.NEW_PROFILE_EXPIRE_MINUTES)

        return created + timedelta(minutes=settings.REGISTRATION_EXPIRE_MINUTES)

    def assert_not_due(self):
        if self.get_expire_time() < timezone.now():
            raise self.DoesNotExist()

    def __str__(self):
        return self.user.name


class UserSiteRegistrationManager(models.Manager):
    def get_or_create(self, **kwargs):
        registration, created = super().get_or_create(**kwargs)
        if not created:
            return registration, created

        maybe_similar = get_similar_display_name_accounts(registration.user, registration.user.name)
        if not maybe_similar.exists():
            return registration, True

        DisplayNameHistory.objects.add_to_history(
            registration.user,
            registration.user.name,
            cause=DisplayNameHistory.CAUSED_BY_LOGIN,
        )
        return registration, True


class UserSiteRegistration(models.Model):
    class Meta:
        ordering = ["-last_login"]

    objects = UserSiteRegistrationManager()

    user = models.ForeignKey("User", on_delete=models.CASCADE, db_index=True, related_name="registered_sites")
    origin_site = models.ForeignKey(
        "OriginSite",
        on_delete=models.CASCADE,
        db_index=True,
        related_name="registered_sites",
    )
    origin_token = models.UUIDField(editable=False, null=True, default=None)
    created_at = models.DateTimeField(auto_now_add=True)
    last_login = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return "{}@{}".format(self.user, self.origin_site)

    @property
    def site_url(self):
        return self.origin_site.origin_site_url

    @property
    def user_name(self):
        return self.user.name


class PreviousLogin(models.Model):
    user = models.ForeignKey("User", on_delete=models.CASCADE, db_index=True, related_name="previous_logins")
    ip = models.GenericIPAddressField(null=True, blank=True, verbose_name="IP")
    user_agent = models.CharField(null=True, blank=True, max_length=200)
    city = models.CharField(null=True, blank=True, max_length=100)
    country = models.CharField(null=True, blank=True, max_length=100)
    latitude = models.DecimalField(max_digits=5, decimal_places=3, null=True)
    longitude = models.DecimalField(max_digits=6, decimal_places=3, null=True)
    last_login_date = models.DateTimeField(default=timezone.now)

    class Meta:
        unique_together = ("user", "ip", "user_agent")

    def add_known_login(self, user):
        session = self.session

        lat_lon = get_lat_lon(session.ip)
        try:
            latitude = lat_lon[0]
            longitude = lat_lon[1]
        except Exception:
            latitude = None
            longitude = None

        login = PreviousLogin.objects.create(
            user=user,
            ip=session.ip,
            user_agent=get_device(session.user_agent),
            city=get_city(session.ip),
            country=get_country(session.ip),
            latitude=latitude,
            longitude=longitude,
        )
        login.save()

    def update_previous_login(self, request):
        try:
            self.last_login_date = timezone.now()
            self.save()
        except Exception:
            pass


class OriginSite(models.Model):
    origin_site_url = models.URLField(null=False, db_index=True, unique=True, verbose_name="URL")
    origin_site_name = models.CharField(null=False, max_length=200, verbose_name="Name")
    origin_site_logo_url = models.ImageField(upload_to=unique_filepath, blank=True, null=True, verbose_name="Logo")
    origin_site_description = models.TextField(null=True, blank=True, verbose_name="Description")
    origin_site_api_token = models.TextField(null=True, blank=True, verbose_name="API Key")
    origin_site_favicon = models.ImageField(null=True, upload_to="favicons", verbose_name="Favicon")

    def url(self, path):
        return "{}/{}".format(self.origin_site_url.rstrip("/"), path.lstrip("/"))

    def favicon_url(self):
        if self.origin_site_favicon.name and os.path.exists(self.origin_site_favicon.path):
            return settings.MEDIA_URL + "/" + self.origin_site_favicon.name
        return static("images/pleio-logo.svg")

    @property
    def hostname(self):
        url = urlparse(self.origin_site_url)
        return url.netloc

    def __str__(self):
        return self.origin_site_name or self.hostname

    class Meta:
        verbose_name = "Origin site"


@receiver(post_delete, sender=OriginSite)
def post_delete_origin_site(sender, instance: OriginSite, **kwargs):
    if instance.origin_site_favicon.name:
        try:
            os.unlink(instance.origin_site_favicon.path)
        except Exception:
            pass


@receiver(pre_save, sender=OriginSite)
def pre_save_origin_site(sender, instance: OriginSite, **kwargs):
    if instance.pk:
        original = OriginSite.objects.filter(pk=instance.pk).first()
        if original and _should_delete_previous_favicon(
            original.origin_site_favicon.name, instance.origin_site_favicon.name
        ):
            try:
                os.unlink(original.origin_site_favicon.path)
            except Exception:
                pass


def _should_delete_previous_favicon(old_name, new_name):
    return not new_name or old_name and new_name != old_name


class EventLog(models.Model):
    ACCOUNT_DELETED = "ACCOUNT_DELETED"
    EMAIL_CHANGE_PENDING = "EMAIL_CHANGE_PENDING"
    EMAIL_CHANGE_CONFIRMED = "EMAIL_CHANGE_CONFIRMED"
    INVALID_COUNTRY_CODE = "INVALID_COUNTRY_CODE"
    INVALID_LOGIN_BACKUP_TOKEN = "INVALID_LOGIN_BACKUP_TOKEN"
    INVALID_LOGIN_TOKEN = "INVALID_LOGIN_TOKEN"
    INVALID_LOGIN_USERID_PASSWORD = "INVALID_LOGIN_USERID_PASSWORD"
    PASSWORD_CHANGED = "PASSWORD_CHANGED"
    PASSWORD_RESET = "PASSWORD_RESET"
    PASSWORD_REQUESTED = "PASSWORD_REQUESTED"
    PASSWORD_SET = "PASSWORD_SET"
    REGISTRATION_CONFIRMED = "REGISTRATION_CONFIRMED"
    REGISTRATION_EMAIL_RESEND = "REGISTRATION_EMAIL_RESEND"
    TFA_ENABLED = "2FA_ENABLED"
    TFA_DISABLED = "2FA_DISABLED"
    USER_INACTIVE_AND_UNKNOWN = "USER_INACTIVE_AND_UNKNOWN"
    USER_LOGGED_IN = "USER_LOGGED_IN"
    USER_LOGGED_OUT = "USER_LOGGED_OUT"
    USER_REGISTERED = "USER_REGISTERED"
    EVENT_TYPE_CHOICES = (
        (ACCOUNT_DELETED, _("account deleted")),
        (EMAIL_CHANGE_PENDING, _("email change pending")),
        (EMAIL_CHANGE_CONFIRMED, _("email change confirmed")),
        (INVALID_COUNTRY_CODE, _("invalid country code")),
        (INVALID_LOGIN_BACKUP_TOKEN, _("invalid_login backup token")),
        (INVALID_LOGIN_TOKEN, _("invalid_login token")),
        (INVALID_LOGIN_USERID_PASSWORD, _("invalid_login userid/password")),
        (PASSWORD_CHANGED, _("password changed")),
        (PASSWORD_RESET, _("password reset")),
        (PASSWORD_REQUESTED, _("password requested")),
        (PASSWORD_SET, _("password set")),
        (REGISTRATION_CONFIRMED, _("registration confirmed")),
        (REGISTRATION_EMAIL_RESEND, _("registration email resend")),
        (TFA_ENABLED, _("2fa enabled")),
        (TFA_DISABLED, _("2fa disabled")),
        (USER_INACTIVE_AND_UNKNOWN, _("user inactive and unknown")),
        (USER_LOGGED_IN, _("user logged in")),
        (USER_LOGGED_OUT, _("user logged out")),
        (USER_REGISTERED, _("user registered")),
    )
    ip = models.GenericIPAddressField(null=True, blank=True, verbose_name="IP")
    userid = models.IntegerField(null=True, default=0)
    email = models.CharField(max_length=255, null=True, blank=True)
    event_type = models.CharField(null=True, blank=True, max_length=30, choices=EVENT_TYPE_CHOICES)
    event_time = models.DateTimeField(default=timezone.now)

    def add_event(self, event_type, user=None):
        ip = None
        email = None
        userid = 0

        if user:
            userid = user.id
            email = user.email or None

        if self:
            session = self.session
            ip = session.ip
            if self.user.is_authenticated:
                userid = self.user.id
                email = self.user.email
            elif email is None:
                email = self.POST.get("username") or session.get("username") or session.get("email")

        event = EventLog.objects.create(
            ip=ip,
            userid=userid,
            email=email,
            event_type=event_type,
        )
        event.save()

    def recaptcha_needed(self):
        time_threshold = timezone.now() - timedelta(minutes=constance_config.RECAPTCHA_MINUTES_THRESHOLD)
        session = self.session
        events = EventLog.objects.filter(
            Q(event_type="INVALID_LOGIN_USERID_PASSWORD")
            | Q(event_type="INVALID_LOGIN_TOKEN")
            | Q(event_type="INVALID_LOGIN_BACKUP_TOKEN")
            | Q(event_type="PASSWORD_REQUESTED")
            | Q(event_type="REGISTRATION_EMAIL_RESEND")
            | Q(event_type="USER_REGISTERED"),
            ip=session.ip,
            event_time__gt=time_threshold,
        )
        return events.count() > constance_config.RECAPTCHA_NUMBER_INVALID_LOGINS


class Page(models.Model):
    page_name = models.CharField(null=False, max_length=50, db_index=True)
    language_code = models.CharField(null=False, max_length=10)
    page_text = models.TextField(null=False)

    def __str__(self):
        return self.page_name + " " + self.language_code

    def get_page(self, page_name, language_code=None):
        page_found = True
        if not page_name:
            page_found = False

        if not language_code:
            language_code = get_language()

        try:
            row = Page.objects.get(page_name=page_name, language_code=language_code)
        except Page.DoesNotExist:
            try:
                row = Page.objects.filter(page_name=page_name)[0]
            except IndexError:
                page_found = False

        if page_found:
            result = mark_safe(row.page_text)
        else:
            result = _("<H1>Page under construction</H1>")

        return mark_safe(result)


class ValidCountry(models.Model):
    country_code = models.CharField(max_length=2, unique=True)
    country_name = models.CharField(max_length=255)
    is_valid_country = models.BooleanField(default=False)

    def valid_country(self=None):
        if constance_config.REGISTRATION_LIMITED_TO_VALID_COUNTRIES is False:
            return True  # no country check required

        if self is None:
            return constance_config.REGISTRATION_ALLOWED_FOR_UNKNOWN_COUNTRIES
        geoip = GeoIP2()

        try:
            country_code = geoip.country_code(self)
        except Exception:
            return constance_config.REGISTRATION_ALLOWED_FOR_UNKNOWN_COUNTRIES

        try:
            return ValidCountry.objects.get(country_code=country_code).is_valid_country
        except ValidCountry.DoesNotExist:
            return constance_config.REGISTRATION_ALLOWED_FOR_UNKNOWN_COUNTRIES


class GovernmentDomain(models.Model):
    name = models.CharField(max_length=100)
    email_domain = models.CharField(max_length=100, unique=True)

    def save(self, *args, **kwargs):
        self.email_domain = self.email_domain.lower()

        super().save(*args, **kwargs)

    def is_email_in_domain(self, email):
        domain = email.split("@")[-1]
        if domain.count(".") > 1:
            domain = "." + domain.split(".")[-2] + "." + domain.split(".")[-1]

        return GovernmentDomain.objects.filter(email_domain=domain).count() > 0


class DisplayNameHistoryManager(models.Manager):
    def add_to_history(self, user, name, cause, original_name=None):
        new_name = self.create(
            user=user,
            name=name,
            cause=cause,
            original_name=original_name or user.name,
        )
        other_records = get_similar_display_name_accounts(user, name)
        if other_records.count():
            new_name.similar_accounts.add(*other_records)
            logger.info("Found %d records attached for %d", new_name.similar_accounts.count(), new_name.pk)
        return new_name


def get_similar_display_name_accounts(user, name):
    my_sites = UserSiteRegistration.objects.filter(user=user).values_list("origin_site_id", flat=True)
    other_accounts = UserSiteRegistration.objects.exclude(user=user)
    result = other_accounts.filter(origin_site_id__in=my_sites, user__name=name)
    logger.info("On changing %s to %s we found %d other records.", user.email, name, result.count())
    return result


class DisplayNameHistory(models.Model):
    UNKNOWN_CAUSE = "unknown_cause"
    CAUSED_BY_UPDATE = "caused_by_update"
    CAUSED_BY_LOGIN = "caused_by_login"
    CAUSES = (
        (UNKNOWN_CAUSE, _("Unknown cause")),
        (CAUSED_BY_UPDATE, _("Caused by display name update")),
        (CAUSED_BY_LOGIN, _("Caused by user login")),
    )

    class Meta:
        verbose_name = _("Display name change")
        verbose_name_plural = _("Display name changes")

    objects = DisplayNameHistoryManager()

    created_at = models.DateTimeField(default=timezone.now)
    name = models.CharField(max_length=256)
    original_name = models.CharField(max_length=256, null=True, blank=True)
    user = models.ForeignKey("core.User", on_delete=models.CASCADE, related_name="name_history")
    similar_accounts = models.ManyToManyField(UserSiteRegistration)
    cause = models.CharField(max_length=32, choices=CAUSES, default=UNKNOWN_CAUSE)

    def __str__(self):
        if self.cause == self.CAUSED_BY_LOGIN:
            return _("Logged in as %(name)s") % {"name": self.name}

        if self.original_name:
            return _("%(original)s to %(name)s") % {
                "original": self.original_name,
                "name": self.name,
            }

        return _("Display name changed to %(name)s") % {"name": self.name}


class ClientWhitelist(models.Model):
    client = models.OneToOneField(
        "oidc_provider.Client",
        on_delete=models.CASCADE,
        related_name="whitelist",
        blank=True,
        null=True,
    )
    name = models.CharField(max_length=255, blank=False, default="New whitelist", verbose_name="Name")
    email_domains = models.TextField(null=True, blank=True, verbose_name="Domains", help_text="One domain per line")
    custom_contact_email = models.EmailField(null=True, blank=True, verbose_name="Custom contact email")
    custom_message = models.TextField(null=True, blank=True, verbose_name="Custom message")

    def save(self, *args, **kwargs):
        self.email_domains = self.email_domains.lower()

        super().save(*args, **kwargs)

    def is_email_in_whitelist(self, email):
        domain = email.split("@")[-1]
        if domain.count(".") > 1:
            domain = "." + domain.split(".")[-2] + "." + domain.split(".")[-1]
        for d in self.email_domains.splitlines():
            if d == domain:
                return True
        return False
