import base64
import os

import requests
from celery import shared_task
from celery.utils import log
from django.core.files.base import ContentFile
from django.core.mail import send_mail as django_send_mail
from django.utils import timezone

from core.favicon import resize_favicon
from core.models import DisplayNameHistory, OriginSite, PendingUser, User
from core.pleio import PleioClient

from .terminate_sessions import terminate_subsite_sessions  # noqa F401

logger = log.get_task_logger(__name__)


@shared_task
def send_mail(subject, message, from_email, recipient_list, **kwargs):
    return django_send_mail(subject, message, from_email, recipient_list, **kwargs)


@shared_task(bind=True, max_retries=5)
def profile_updated_signal(self, user_id):
    try:
        errors = False
        user = User.objects.get(id=user_id)
        for registration in user.registered_sites.all():
            try:
                client = PleioClient(registration.origin_site)
                response = client.post("api/profile/update/", data={"id": user_id})
                if not response.ok:
                    logger.error(response.reason)
                    errors = True
            except Exception as e:
                logger.error(msg=str(e))
                errors = True
        assert not errors, "Errors during notify pleio sites."
    except Exception:
        self.retry(countdown=60)


@shared_task
def refresh_site_favicon(site_id):
    site = OriginSite.objects.get(pk=site_id)
    client = PleioClient(site)
    response = client.get("api/site_info/")

    if response.status_code == 200:
        fields = response.json()

        if fields["favicon"]:
            site.origin_site_favicon = ContentFile(
                base64.decodebytes(fields["favicon_data"].encode()), fields["favicon"]
            )
        else:
            site.origin_site_favicon = None

        site.save()
        site.refresh_from_db()

        if site.origin_site_favicon.name:
            resize_favicon(site.origin_site_favicon.path)
            assert os.path.exists(site.origin_site_favicon.path), "Failed to resize favicon file"
    else:
        logger.error(
            "Fetch site info: response status_code=%s",
            site.origin_site_url,
            response.status_code,
        )


@shared_task
def refresh_user_avatar(user_id):
    from core.models import User

    user: User = User.objects.get(pk=user_id)

    # Create new thumbnails
    try:
        user.process_avatar()
        user.update_avatar_name()
    except FileNotFoundError:
        user.avatar.name = ""
        user.stored_avatar = ""
        user.save()

    # Then loop all subsites of the user to signal the subsites to refresh the avatar.
    profile_updated_signal(user_id)


@shared_task(bind=True, max_retries=10)
def ban_user(self, pk, email, reason, site_id=None):
    if not site_id:
        for site in OriginSite.objects.all():
            ban_user.delay(pk, email, reason, site.pk)
    else:
        try:
            site = OriginSite.objects.get(id=site_id)
            client = PleioClient(site)
            client.logger = logger
            client.post(
                "api/profile/ban/",
                {
                    "reason": reason,
                    "id": pk,
                    "email": email,
                },
            )
        except (ConnectionRefusedError, requests.exceptions.ConnectionError):
            logger.error("Could not reach %s", site.origin_site_url if site else site_id)
            if self.request.retries < self.max_retries:
                self.retry(countdown=(60 * (self.request.retries + 1)))


@shared_task
def cleanup_display_name_history():
    threshold = timezone.now() - timezone.timedelta(days=30)
    DisplayNameHistory.objects.filter(created_at__lte=threshold).delete()


@shared_task(ignore_result=True)
def cleanup_pending_users():
    PendingUser.objects.filter_expired_pending_users().delete()


@shared_task(ignore_result=True)
def cleanup_users():
    for user in User.objects.filter_expired_users():
        user.delete()
