import requests
from celery import shared_task
from celery.utils import log

from core.models import OriginSite
from core.pleio import PleioClient

logger = log.get_task_logger(__name__)


@shared_task(bind=True, max_retries=10)
def terminate_subsite_sessions(self, pk, email, site_id=None):
    try:
        if not site_id:
            logger.info("Schedule terminate session for all subsites")
            for site in OriginSite.objects.all():
                terminate_subsite_sessions.delay(pk, email, site.pk)

        else:
            logger.info("Enter terminate_subsite_sessions for site %s", site_id)
            _terminate_remote_sessions(pk, email, site_id)
    except RetryTerminateSessionsError as e:
        logger.error("%s", str(e))
        if self.request.retries < self.max_retries:
            self.retry(countdown=(60 * (self.request.retries + 1)))


class RetryTerminateSessionsError(Exception):
    pass


def _terminate_remote_sessions(pk, email, site_id):
    try:
        # Load site based on id
        site = OriginSite.objects.get(id=site_id)
    except OriginSite.DoesNotExist:
        logger.error("Site with id %s not found", site_id)
        return

    try:
        client = PleioClient(site)
        client.logger = logger
        client.post(
            "api/profile/terminate_session/",
            {
                "id": pk,
                "email": email,
            },
        )
    except (ConnectionRefusedError, requests.exceptions.ConnectionError):
        msg = f"Could not reach {site.origin_site_url}"
        raise RetryTerminateSessionsError(msg)
