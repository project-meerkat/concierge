import ipaddress
import logging
import re
from urllib.parse import urlencode, urljoin

from django.db.models import Q
from django.urls import reverse
from django.utils.module_loading import import_string

logger = logging.getLogger(__name__)


class FromEmail:
    """
    Helper class to create a from email address for situations where
    either the name or the email address must be overridden and may
    or may not be present in the input.
    """

    def __init__(self, from_mail):
        self.from_mail = from_mail or ""

        self.name, self.email = self._extract_name()

    def _extract_name(self):
        match = re.match(r"^(.*?)<(.*)>$", self.from_mail)
        if match:
            return match[1].strip(), match[2].strip()
        return None, self.from_mail

    def __str__(self):
        if self.name:
            return "{name} <{email}>".format(name=self.name, email=self.email)
        return self.email


class Reverse:
    """
    Helper class to create urls by means of django.urls.reverse
    Copy specific GET parameters in the new url.
    Look for those parameters in 3 bins, in the following order:
        1. self.override
        2. self.request.POST
        3. self.request.GET
    """

    def __init__(self, request=None, expected_parameters=None, override_parameters=None):
        # Allow read-only parameters.
        self.override = override_parameters or {}

        # prevent exposure of all parameters in "request.POST".
        self.expected_parameters = list({*(expected_parameters or []), *(self.override.keys())})

        # Negotiate GET and POST parameters.
        self.request = request

    def __call__(self, *args, post=True, **kwargs):
        parameters = {}
        for key in sorted({*self.expected_parameters, *self.request.GET}):
            if key in self.override:
                parameters[key] = self.override.get(key) or ""
            elif post and key in self.request.POST:
                parameters[key] = self.request.POST.get(key) or ""
            elif key in self.request.GET:
                parameters[key] = self.request.GET.get(key) or ""

        parameters = {k: v for k, v in parameters.items() if bool(v)}

        if parameters:
            from urllib.parse import urlencode

            return reverse(*args, **kwargs) + "?" + urlencode(parameters)

        return reverse(*args, **kwargs)


def build_url(*args, **kwargs):
    kwargs = {k: v for k, v in kwargs.items() if v}
    parameters = "?" + urlencode(kwargs) if kwargs else ""
    return urljoin(*args) + parameters


def mimetype_from_filename(filename):
    """
    Return a mimetype based on the filename extension.
    """
    import mimetypes

    return mimetypes.guess_type(filename)[0] or "application/octet-stream"


def url_is_same_site(request, url):
    url = url or ""

    if url.startswith("//"):
        return False

    this_site = request.build_absolute_uri("/")
    if url.startswith(this_site):
        return True

    return url.startswith("/")


def force_same_site_url(request, url):
    """
    Pass through relative (to local) URLs, otherwise return '/'.
    """
    if url_is_same_site(request, url):
        return url
    return "/"


def get_session_model():
    from django.conf import settings

    if not hasattr(settings, "SESSION_ENGINE"):
        return None

    match settings.SESSION_ENGINE:
        case "django.contrib.sessions.backends.db":
            return import_string("django.contrib.sessions.models.Session")
        case "user_sessions.backends.db":
            return import_string("user_sessions.models.Session")

    return None


def terminate_local_sessions(pk, email):
    """
    End sessions in other browsers.

    Keep in mind that the session in which the password is updated is not killed.
    """
    model = get_session_model()
    assert model, "Can't end local sessions for no session model has been found."

    sessions = model.objects.filter(Q(user_id=pk) | Q(user__email__iexact=email))
    sessions.delete()


def get_client_ip(request):
    x_forwarded_for = request.META.get("HTTP_X_FORWARDED_FOR")

    try:
        ipv4_version = ipaddress.IPv6Address(x_forwarded_for).ipv4_mapped
        if ipv4_version:
            x_forwarded_for = str(ipv4_version)
    except ipaddress.AddressValueError:
        pass

    if x_forwarded_for:
        ip = x_forwarded_for.split(",")[0]
    else:
        ip = request.META.get("REMOTE_ADDR")
    return ip
