# Generated by Django 3.2.23 on 2024-02-09 13:24

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0060_auto_20231113_1242"),
    ]

    operations = [
        migrations.DeleteModel(
            name="ResizedAvatars",
        ),
    ]
