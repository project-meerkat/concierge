# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-07-12 15:44
from __future__ import unicode_literals

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0029_eventlog_user_email"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="eventlog",
            name="user_email",
        ),
        migrations.AddField(
            model_name="eventlog",
            name="user",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                to=settings.AUTH_USER_MODEL,
            ),
            preserve_default=False,
        ),
    ]
