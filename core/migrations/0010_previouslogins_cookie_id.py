# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-10-22 09:23
from __future__ import unicode_literals

import uuid

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0009_auto_20171022_1049"),
    ]

    operations = [
        migrations.AddField(
            model_name="previouslogins",
            name="cookie_id",
            field=models.UUIDField(default=uuid.uuid4, editable=False, unique=True),
        ),
    ]
