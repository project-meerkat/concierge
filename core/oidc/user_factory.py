import re

from core.models import User
from saml.models import ExternalId

provider_user_factories = {
    # add factories here for OIDC providers that can obtain more info than just email
}

# A list of OIDC claims containing user name information. The order of this list determines
# which claim is prioritized over another to be taken as the username.
OIDC_NAME_CLAIMS = [
    "preferred_username",
    "nickname",
    "name",
    "given_name",
]


def get_provider_user_factory(provider):
    return provider_user_factories.get(provider.shortname, EmailUserFactory(provider))


class EmailUserFactory:
    def __init__(self, provider):
        self.provider = provider

    def create_user(self, claims):
        email = claims["email"].lower()

        try:
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            user = User.objects.create_user(
                name=self.retrieve_user_name_from_claims(claims),
                email=email,
            )

        user.is_active = True
        user.save()

        ExternalId.objects.create(identityproviderid=self.provider, externalid=claims["sub"], userid=user)

        return user

    def update_user(self, user, claims):
        user.email = claims["email"]
        user.name = self.retrieve_user_name_from_claims(claims)
        user.save()
        return user

    def get_user_name_from_email(self, email):
        name = email[: email.index("@")]
        return re.sub("[^0-9a-zA-z]+", " ", name)

    def retrieve_user_name_from_claims(self, claims):
        """Go through the given claims to check if there is any claim containing information
        regarding the user's name. If not, a name is inferred from the user's email."""
        for claim in OIDC_NAME_CLAIMS:
            if claim in claims:
                return claims[claim]
        return self.get_user_name_from_email(claims["email"])
