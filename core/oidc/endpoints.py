import hashlib
import logging
from base64 import urlsafe_b64encode
from urllib.parse import parse_qs, urlparse, urlsplit, urlunsplit

from oidc_provider.lib.endpoints.authorize import AuthorizeEndpoint
from oidc_provider.lib.endpoints.token import TokenEndpoint
from oidc_provider.lib.errors import RedirectUriError, TokenError
from oidc_provider.models import Code
from urlmatch import urlmatch

logger = logging.getLogger(__name__)


def is_valid_redirect(redirect_uris, url):
    try:
        # this removes the query and fragment from the url to prevent attacks like https://attacker.com?https://www.site.nl/
        url = urlunsplit(urlsplit(url)._replace(query="", fragment=""))

        for allowed_uri in redirect_uris:
            if urlmatch(allowed_uri, url):
                return True
    except Exception:
        pass
    return False


class PleioAuthorizeEndpoint(AuthorizeEndpoint):
    """
    Override validate_params to allow wildcards
    """

    def validate_params(self):
        try:
            super().validate_params()
        except RedirectUriError:
            if not is_valid_redirect(self.client.redirect_uris, self.params["redirect_uri"]):
                logger.info(
                    "[PleioAuthorizeEndpoint] Invalid redirect uri: %s",
                    self.params["redirect_uri"],
                )
                raise RedirectUriError()

    def create_response_uri(self):
        """Override to add the code the user's session"""
        uri = super().create_response_uri()
        query_content = parse_qs(urlparse(uri).query)
        if "code" in query_content:
            self.request.session["code"] = query_content["code"][0]
            self.request.session.save()

        return uri


class PleioTokenEndpoint(TokenEndpoint):
    """
    Override validate_params to allow wildcards
    """

    def validate_params(self):
        try:
            super().validate_params()
        except TokenError as error:
            if self.params["grant_type"] == "authorization_code":
                if not is_valid_redirect(self.client.redirect_uris, self.params["redirect_uri"]):
                    logger.debug("[Token] Invalid redirect uri: %s", self.params["redirect_uri"])
                    msg = "invalid_client"
                    raise TokenError(msg)

                try:
                    self.code = Code.objects.get(code=self.params["code"])
                except Code.DoesNotExist:
                    logger.debug("[Token] Code does not exist: %s", self.params["code"])
                    msg = "invalid_grant"
                    raise TokenError(msg)

                if not (self.code.client == self.client) or self.code.has_expired():
                    logger.debug("[Token] Invalid code: invalid client or code has expired")
                    msg = "invalid_grant"
                    raise TokenError(msg)

                # Validate PKCE parameters.
                if self.params["code_verifier"]:
                    if self.code.code_challenge_method == "S256":
                        new_code_challenge = (
                            urlsafe_b64encode(hashlib.sha256(self.params["code_verifier"].encode("ascii")).digest())
                            .decode("utf-8")
                            .replace("=", "")
                        )
                    else:
                        new_code_challenge = self.params["code_verifier"]

                    # TODO: We should explain the error.
                    if not (new_code_challenge == self.code.code_challenge):
                        msg = "invalid_grant"
                        raise TokenError(msg)
            else:
                raise error

    def create_code_response_dic(self):
        """
        Override to include the user's session in the user model before calling create_id_token
        """

        session = None
        for ses in self.code.user.session_set.all():
            decoded = ses.get_decoded()
            if decoded.get("code", None) == self.code.code:
                session = ses

        self.code.user.session = session.get_decoded() if session is not None else {}
        dic = super().create_code_response_dic()

        return dic
