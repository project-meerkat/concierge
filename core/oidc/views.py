import logging
import time
from urllib.parse import urlparse

from django.contrib import auth
from django.contrib.auth import authenticate
from django.contrib.auth import logout as django_user_logout
from django.core.exceptions import SuspiciousOperation
from django.shortcuts import render
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils import timezone
from django.utils.crypto import get_random_string
from django.utils.http import urlencode
from django.views.generic import View
from mozilla_django_oidc.utils import absolutify
from mozilla_django_oidc.views import OIDCAuthenticationCallbackView
from oidc_provider import settings
from oidc_provider.compat import get_attr_or_callable
from oidc_provider.lib.errors import (
    AuthorizeError,
    ClientIdError,
    RedirectUriError,
    TokenError,
    UserAuthError,
)
from oidc_provider.lib.utils.authorize import strip_prompt_login
from oidc_provider.lib.utils.common import redirect
from oidc_provider.views import AuthorizeView, TokenView, redirect_to_login

from saml.models import OidcIdentityProvider

from ..models import OriginSite, UserSiteRegistration
from .endpoints import PleioAuthorizeEndpoint, PleioTokenEndpoint

OIDC_TEMPLATES = settings.get("OIDC_TEMPLATES")

logger = logging.getLogger(__name__)


class PleioAuthorizeView(AuthorizeView):
    authorize_endpoint_class = PleioAuthorizeEndpoint

    # Customized get function from AuthorizeView parent to start auth requests for external OIDC providers
    def get(self, request, *args, **kwargs):
        authorize = self.authorize_endpoint_class(request)

        try:
            authorize.validate_params()

            if get_attr_or_callable(request.user, "is_authenticated"):
                # Check if client has whitelisted email domains
                if hasattr(authorize.client, "whitelist"):
                    if not authorize.client.whitelist.is_email_in_whitelist(request.user.email):
                        uri = urlparse(authorize.params["redirect_uri"])
                        context = {
                            "requested_site": "{uri.scheme}://{uri.netloc}".format(uri=uri),
                            "custom_contact_email": authorize.client.whitelist.custom_contact_email,
                            "custom_message": authorize.client.whitelist.custom_message,
                        }
                        return render(request, "login/whitelist_blocked.html", context)

                # Check if there's a hook setted.
                hook_resp = settings.get("OIDC_AFTER_USERLOGIN_HOOK", import_str=True)(
                    request=request, user=request.user, client=authorize.client
                )
                if hook_resp:
                    return hook_resp

                if "login" in authorize.params["prompt"]:
                    if "none" in authorize.params["prompt"]:
                        raise AuthorizeError(
                            authorize.params["redirect_uri"],
                            "login_required",
                            authorize.grant_type,
                        )
                    else:
                        django_user_logout(request)
                        next_page = strip_prompt_login(request.get_full_path())
                        return redirect_to_login(next_page, settings.get("OIDC_LOGIN_URL"))

                if "select_account" in authorize.params["prompt"]:
                    # TODO: see how we can support multiple accounts for the end-user.
                    if "none" in authorize.params["prompt"]:
                        raise AuthorizeError(
                            authorize.params["redirect_uri"],
                            "account_selection_required",
                            authorize.grant_type,
                        )
                    else:
                        django_user_logout(request)
                        return redirect_to_login(request.get_full_path(), settings.get("OIDC_LOGIN_URL"))

                if {"none", "consent"}.issubset(authorize.params["prompt"]):
                    raise AuthorizeError(
                        authorize.params["redirect_uri"],
                        "consent_required",
                        authorize.grant_type,
                    )

                implicit_flow_resp_types = {"id_token", "id_token token"}
                allow_skipping_consent = (
                    authorize.client.client_type != "public"
                    or authorize.params["response_type"] in implicit_flow_resp_types
                )

                if not authorize.client.require_consent and (
                    allow_skipping_consent and "consent" not in authorize.params["prompt"]
                ):
                    return redirect(authorize.create_response_uri())

                if authorize.client.reuse_consent:
                    # Check if user previously give consent.
                    if authorize.client_has_user_consent() and (
                        allow_skipping_consent and "consent" not in authorize.params["prompt"]
                    ):
                        return redirect(authorize.create_response_uri())

                if "none" in authorize.params["prompt"]:
                    raise AuthorizeError(
                        authorize.params["redirect_uri"],
                        "consent_required",
                        authorize.grant_type,
                    )

                # Generate hidden inputs for the form.
                context = {
                    "params": authorize.params,
                }
                hidden_inputs = render_to_string("oidc_provider/hidden_inputs.html", context)

                # Remove `openid` from scope list
                # since we don't need to print it.
                if "openid" in authorize.params["scope"]:
                    authorize.params["scope"].remove("openid")

                context = {
                    "client": authorize.client,
                    "hidden_inputs": hidden_inputs,
                    "params": authorize.params,
                    "scopes": authorize.get_scopes_information(),
                }

                return render(request, OIDC_TEMPLATES["authorize"], context)
            else:
                if "provider" in request.GET:
                    provider = request.GET["provider"]
                    if OidcIdentityProvider.objects.filter(shortname=provider).exists():
                        request.session["oidc_login_next"] = request.get_full_path()
                        return redirect(
                            reverse(
                                "oidc_authentication_init",
                                kwargs={"provider": provider},
                            )
                        )
                    else:
                        logger.warning('Unknown provider "%s", ignoring parameter', provider)

                if "none" in authorize.params["prompt"]:
                    raise AuthorizeError(
                        authorize.params["redirect_uri"],
                        "login_required",
                        authorize.grant_type,
                    )
                if "login" in authorize.params["prompt"]:
                    next_page = strip_prompt_login(request.get_full_path())
                    return redirect_to_login(next_page, settings.get("OIDC_LOGIN_URL"))

                if self.request.GET.get("register") == "1":
                    return redirect(reverse("register") + "?" + urlencode({"next": request.get_full_path()}))

                return redirect_to_login(request.get_full_path())

        except (ClientIdError, RedirectUriError) as error:
            context = {
                "error": error.error,
                "description": error.description,
            }

            return render(request, OIDC_TEMPLATES["error"], context)

        except AuthorizeError as error:
            uri = error.create_uri(authorize.params["redirect_uri"], authorize.params["state"])

            return redirect(uri)


class PleioTokenView(TokenView):
    def post(self, request, *args, **kwargs):
        token = PleioTokenEndpoint(request)

        try:
            token.validate_params()

            if request.POST.get("origin_token"):
                origin_site, created = OriginSite.objects.get_or_create(origin_site_url=request.POST["origin_url"])
                if created:
                    from core.tasks import refresh_site_favicon

                    refresh_site_favicon.delay(origin_site.id)
                if "origin_name" in request.POST:
                    origin_site.origin_site_name = request.POST["origin_name"]
                if "origin_description" in request.POST:
                    origin_site.origin_site_description = request.POST["origin_description"]
                if "origin_api_token" in request.POST:
                    origin_site.origin_site_api_token = request.POST["origin_api_token"]
                origin_site.save()

                registration, _ = UserSiteRegistration.objects.get_or_create(
                    origin_site=origin_site, user=token.code.user
                )
                registration.origin_token = request.POST["origin_token"]
                if "registration_date" in request.POST:
                    registration.created_at = request.POST["registration_date"]
                registration.last_login = timezone.now()
                registration.save()

            dic = token.create_response_dic()

            return PleioTokenEndpoint.response(dic)

        except TokenError as error:
            return PleioTokenEndpoint.response(error.create_dict(), status=400)
        except UserAuthError as error:
            return PleioTokenEndpoint.response(error.create_dict(), status=403)


class PleioAuthenticateView(View):
    def get(self, request, provider):
        try:
            provider_settings = OidcIdentityProvider.objects.get(shortname=provider)
        except OidcIdentityProvider.DoesNotExist:
            raise SuspiciousOperation("The user should not reach this page for a nonexistent provider: " + provider)

        nonce = get_random_string(32)
        state = get_random_string(32)
        redirect_uri = absolutify(
            self.request,
            reverse("oidc_authentication_callback", kwargs={"provider": provider}),
        )
        client_id = provider_settings.client_id

        params = {
            "response_type": "code",
            "scope": "openid email",
            "client_id": client_id,
            "redirect_uri": redirect_uri,
            "state": state,
            "nonce": nonce,
        }

        states = request.session.get("oidc_states", {})

        states[state] = {"nonce": nonce, "added_on": time.time()}
        # entire object has to be updated or it won't update in the db
        request.session["oidc_states"] = states

        query = urlencode(params)
        url = provider_settings.authorize_endpoint

        return redirect("{url}?{query}".format(url=url, query=query))


class PleioCallbackView(OIDCAuthenticationCallbackView):
    # Extended to also provide the provider as an argument to the AuthenticationBackend

    def get(self, request, provider):
        """Callback handler for OIDC authorization code flow"""

        if request.GET.get("error"):
            # Ouch! Something important failed.
            # Make sure the user doesn't get to continue to be logged in
            # otherwise the refresh middleware will force the user to
            # redirect to authorize again if the session refresh has
            # expired.
            if request.user.is_authenticated:
                auth.logout(request)
            assert not request.user.is_authenticated
        elif "code" in request.GET and "state" in request.GET:
            # Check instead of "oidc_state" check if the "oidc_states" session key exists!
            if "oidc_states" not in request.session:
                return self.login_failure()

            # State and Nonce are stored in the session "oidc_states" dictionary.
            # State is the key, the value is a dictionary with the Nonce in the "nonce" field.
            state = request.GET.get("state")
            if state not in request.session["oidc_states"]:
                msg = "OIDC callback state not found in session `oidc_states`!"
                raise SuspiciousOperation(msg)

            # Get the nonce from the dictionary for further processing and delete the entry to
            # prevent replay attacks.
            nonce = request.session["oidc_states"][state]["nonce"]
            del request.session["oidc_states"][state]

            # Authenticating is slow, so save the updated oidc_states.
            request.session.save()
            # Reset the session. This forces the session to get reloaded from the database after
            # fetching the token from the OpenID connect provider.
            # Without this step we would overwrite items that are being added/removed from the
            # session in parallel browser tabs.
            request.session = request.session.__class__(request.session.session_key)

            kwargs = {
                "request": request,
                "nonce": nonce,
                "provider": provider,
            }

            self.user = authenticate(**kwargs)

            if self.user and self.user.is_active:
                request.session["via_sso"] = True
                try:
                    request.session["oidc_logout"] = OidcIdentityProvider.objects.get(
                        shortname=provider
                    ).end_session_endpoint
                except OidcIdentityProvider.DoesNotExist:
                    request.session["oidc_logout"] = None
                request.session.save()
                return self.login_success()
        return self.login_failure()
