from django import http
from django.template import TemplateDoesNotExist, loader
from django.views.decorators.csrf import requires_csrf_token
from django.views.defaults import ERROR_500_TEMPLATE_NAME


@requires_csrf_token
def error_500(request, template_name="500.html"):
    """
    custom 500 error handler.

    overrides default.server_error to provide log_uuid in template
    """
    try:
        template = loader.get_template(template_name)
    except TemplateDoesNotExist:
        if template_name != ERROR_500_TEMPLATE_NAME:
            # Reraise if it's a missing custom template.
            raise

        return http.HttpResponseServerError("<h1>Server Error (500)</h1>", content_type="text/html")

    return http.HttpResponseServerError(template.render({"log_uuid": request.session.get("log_uuid")}))
