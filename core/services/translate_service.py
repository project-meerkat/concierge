import json
import logging
import warnings

import requests
from django.conf import settings
from requests import HTTPError

from core.services.shared import BaseTranslatorService

logger = logging.getLogger(__name__)


class DeeplTranslatorService(BaseTranslatorService):
    def __init__(self):
        self.client = DeeplClient()
        self.client.LOGGING_ENABLED = True

    def translate_string(self, text, target_language, source_language="en"):
        return self.client.translate(text, target_language, source_language)

    def translate_strings(self, strings, target_language, source_language="en", optimized=True):
        result = []

        for s in strings:
            text = self.translate_string(s, target_language, source_language)
            result.append(text)

        return tuple(result)


class DeeplClient:
    LOGGING_ENABLED = False

    def __init__(self, url=None, auth_key=None):
        self.url = url or settings.DEEPL_URL
        assert self.url, "Add DEEPL_URL=https://api-free.deepl.com/v2/translate to your environment"
        self.auth_key = auth_key or settings.DEEPL_TOKEN
        assert self.auth_key, "Add DEEPL_TOKEN=[token, probably ending :fx, goes here] to your environment"

    def log(self, msg):
        if self.LOGGING_ENABLED:
            warnings.warn(msg, stacklevel=1)

    def translate(self, text, target_lang, source_lang, **kwargs):
        try:
            self.log("Doing (payed) deepl request")
            if "formality" not in kwargs and target_lang.upper() == "NL":
                kwargs["formality"] = "prefer_less"

            response = requests.get(
                self.url,
                data={
                    **kwargs,
                    "text": text,
                    "auth_key": self.auth_key,
                    "source_lang": source_lang.upper()[0:2],
                    "target_lang": target_lang.upper(),
                },
                timeout=10,
            )

            self.log(response.text)

            response.raise_for_status()
            return json.loads(response.text)["translations"][0]["text"]
        except HTTPError:
            logger.error(
                "Error while translating %(text)s. Have you set it up properly? Url: %(url)s, Key: %(key)s",
                {"text": text, "url": self.url, "key": kwargs["auth_key"]},
            )
            return ""
