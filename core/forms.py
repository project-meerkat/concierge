import logging

from django import forms
from django.conf import settings
from django.contrib.auth import authenticate, password_validation
from django.contrib.auth.forms import PasswordResetForm as PasswordResetFormBase
from django.utils.translation import gettext_lazy as _
from django_otp.forms import OTPTokenForm
from two_factor.forms import TOTPDeviceForm
from two_factor.utils import default_device

from core import messages
from emailvalidator.validator import is_email_valid
from floodctl.exceptions import FloodOverflowError
from floodctl.models import FloodLog

from .helpers import verify_captcha_response
from .login_session_helpers import is_name_valid
from .models import DisplayNameHistory, EventLog, Page, User

logger = logging.getLogger(__name__)


class EmailField(forms.EmailField):
    def clean(self, value):
        super(EmailField, self).clean(value)
        if not is_email_valid(value):
            raise forms.ValidationError(_("Your email address is not allowed."))

        return value


class RecaptchaForm(forms.Form):
    def __init__(self, request, *args, **kwargs):
        add_recaptcha = kwargs.pop("add_reCAPTCHA", None)
        super().__init__(*args, **kwargs)

        if add_recaptcha:
            # Create field like this because of the dashes in the name
            self.fields["g-recaptcha-response"] = forms.CharField()

    def recaptcha_required(self):
        return self.fields.get("g-recaptcha-response") is not None

    def clean(self):
        super().clean()

        if self.fields.get("g-recaptcha-response"):
            g_recaptcha_response = self.cleaned_data.get("g-recaptcha-response")
            if not verify_captcha_response(g_recaptcha_response):
                raise forms.ValidationError(
                    messages.CAPTCHA_MISMATCH,
                    code="captcha_mismatch",
                )

        return self.cleaned_data


class RegisterForm(RecaptchaForm):
    name = forms.CharField(
        required=True,
        max_length=100,
        widget=forms.TextInput(attrs={"autofocus": "autofocus", "autocomplete": "name"}),
        error_messages={"required": messages.NAME_REQUIRED},
    )
    email = EmailField(
        required=True,
        widget=forms.TextInput(attrs={"autocomplete": "email"}),
        error_messages={
            "required": messages.EMAIL_REQUIRED,
            "invalid": messages.EMAIL_INVALID,
        },
    )
    password1 = forms.CharField(
        strip=False,
        widget=forms.PasswordInput(attrs={"autocomplete": "new-password"}),
        error_messages={"required": messages.PASSWORD_REQUIRED},
    )
    password2 = forms.CharField(
        strip=False,
        widget=forms.PasswordInput(attrs={"autocomplete": "new-password"}),
        error_messages={"required": messages.PASSWORD_REQUIRED},
    )
    accepted_terms = forms.BooleanField(
        required=True,
        error_messages={"required": messages.ACCEPTED_TERMS_REQUIRED},
    )
    next = forms.CharField(widget=forms.HiddenInput(), required=False)

    def __init__(self, request, *args, **kwargs):
        kwargs["add_reCAPTCHA"] = getattr(settings, "GOOGLE_RECAPTCHA_SITE_KEY", None)

        super(RegisterForm, self).__init__(request, *args, **kwargs)

    def clean(self):
        super(RegisterForm, self).clean()

        return self.cleaned_data

    def clean_email(self):
        email = self.cleaned_data.get("email").lower()

        return email

    def clean_name(self):
        name = self.cleaned_data.get("name")

        if not is_name_valid(name):
            raise forms.ValidationError(
                messages.FIRST_LAST_NAME_INVALID,
                code="name_invalid",
            )

        return name

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")

        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                messages.PASSWORD_MISMATCH,
                code="password_mismatch",
            )

        password_validation.validate_password(self.cleaned_data.get("password2"))
        return password2


class UserProfileForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ("name", "email", "avatar")

    error_messages = {
        "duplicate_email": _("This email is already registered."),
        "duplicate_username": _("This username is already registered."),
        "name_invalid": _("The name contains invalid characters."),
    }

    def __init__(self, *args, **kwargs):
        self.form_user = kwargs.get("instance", None)
        self.current_user = User.objects.get(pk=self.form_user.pk)
        self.name_save = self.current_user.name
        self.email_save = self.current_user.email
        self.new_email_save = self.current_user.new_email

        super(UserProfileForm, self).__init__(*args, **kwargs)

        if "name" in self.fields:
            self.fields["name"].widget.attrs["autocomplete"] = "name"
        if "email" in self.fields:
            self.fields["email"].widget.attrs["autocomplete"] = "email"

    def clean_name(self):
        name = self.cleaned_data.get("name")

        if name == self.name_save:
            return name

        if name != self.name_save:
            DisplayNameHistory.objects.add_to_history(
                user=self.current_user,
                name=name,
                cause=DisplayNameHistory.CAUSED_BY_UPDATE,
            )

        if not is_name_valid(name):
            raise forms.ValidationError(
                messages.NAME_INVALID,
                code="name_invalid",
            )

        return name

    def clean_email(self):
        email = self.cleaned_data.get("email").lower()

        if email != self.email_save:
            try:
                user = User.objects.get(email=email)
            except Exception:
                user = None

            if user:
                raise forms.ValidationError(
                    messages.DUPLICATE_EMAIL,
                    code="duplicate_email",
                )
            else:
                self.current_user.new_email = email
                email = self.email_save
        else:
            self.current_user.new_email = None

        return email

    def clean_new_email(self):
        try:
            new_email = self.current_user.new_email.lower()
        except AttributeError:
            new_email = None
        return new_email

    def clean(self):
        super(UserProfileForm, self).clean()

        self.cleaned_data["new_email"] = self.clean_new_email()


class CompleteProfileForm(UserProfileForm):
    class Meta:
        model = User
        fields = ("name", "avatar", "accepted_terms")

    def __init__(self, *args, require_terms=False, **kwargs):
        super().__init__(*args, **kwargs)
        self.require_terms = require_terms

    def clean_accepted_terms(self):
        accepted_terms = self.cleaned_data.get("accepted_terms")
        if self.require_terms and not accepted_terms:
            raise forms.ValidationError(
                messages.ACCEPTED_TERMS_REQUIRED,
                code="required_field",
            )
        return accepted_terms


class UsernameInputForm(RecaptchaForm):
    username = forms.CharField(
        max_length=254,
        widget=forms.TextInput(attrs={"autocomplete": "email"}),
        required=True,
        error_messages={"required": messages.EMAIL_REQUIRED},
    )


class AuthenticationForm(RecaptchaForm):
    username = forms.CharField(
        max_length=254,
        widget=forms.TextInput(attrs={"autocomplete": "email"}),
        required=True,
        error_messages={"required": messages.EMAIL_REQUIRED},
    )
    password = forms.CharField(
        label=_("Password"),
        widget=forms.PasswordInput(attrs={"autocomplete": "current-password"}),
        error_messages={"required": messages.PASSWORD_REQUIRED},
    )

    error_messages = {
        "banned": "banned",
        "invalid_login": "invalid_login",
        "inactive": "inactive",
        "captcha_mismatch": _("captcha_mismatch"),
        "flood_control_error": _("Too many failed authentication attempts."),
    }

    def __init__(self, request, *args, **kwargs):
        self.request = request
        self.user_cache = None
        kwargs["add_reCAPTCHA"] = getattr(settings, "GOOGLE_RECAPTCHA_SITE_KEY", None) and EventLog.recaptcha_needed(
            request
        )
        super().__init__(request, *args, **kwargs)

    def clean_username(self):
        if input_username := self.cleaned_data.get("username"):
            return self.cleaned_data.get("username").lower().strip()
        return input_username

    def clean(self):
        super().clean()
        username = self.cleaned_data.get("username")
        password = self.cleaned_data.get("password")

        if username and password:
            try:
                FloodLog.objects.assert_not_blocked(self.request, username)
                user = User.objects.get(email__iexact=username)
            except User.DoesNotExist:
                FloodLog.objects.add_record(self.request, username)
                raise forms.ValidationError(
                    self.error_messages["invalid_login"],
                    code="invalid_login",
                    params={"username": username},
                )
            except FloodOverflowError:
                raise forms.ValidationError(
                    self.error_messages["flood_control_error"],
                    code="flood_control_error",
                )

            if not user.is_active:
                FloodLog.objects.add_record(self.request, username)
                raise forms.ValidationError(
                    self.error_messages["inactive"],
                    code="inactive",
                    params={"username": username},
                )

            if user.is_banned:
                FloodLog.objects.add_record(self.request, username)
                raise forms.ValidationError(
                    self.error_messages["banned"],
                    code="banned",
                    params={"username": username},
                )

            email = user.email
            self.user_cache = authenticate(self.request, email=email, password=password)
            if self.user_cache is None:
                FloodLog.objects.add_record(self.request, username)
                raise forms.ValidationError(
                    self.error_messages["invalid_login"],
                    code="invalid_login",
                    params={"username": username},
                )
            else:
                FloodLog.objects.reset_target(self.request, username)

        return self.cleaned_data

    def get_user(self):
        return self.user_cache


class AuthenticationTokenForm(OTPTokenForm):
    otp_token = forms.IntegerField(
        label=_("Token"),
        widget=forms.TextInput(attrs={"autofocus": True, "autocomplete": "off"}),
    )
    otp_device = forms.ChoiceField(choices=[], required=False)

    def __init__(self, user, request, **kwargs):
        super().__init__(user, request, **kwargs)
        self.request = request

    def clean(self):
        self.clean_otp(self.user)
        return self.cleaned_data

    def clean_otp(self, user):
        if user is None:
            return

        device = default_device(user)
        token = self.cleaned_data.get("otp_token")

        user.otp_device = None
        flood_target = "%s|2fa" % self.user.email

        try:
            FloodLog.objects.assert_not_blocked(self.request, flood_target)

            if self.cleaned_data.get("otp_challenge"):
                self._handle_challenge(device)
            elif token:
                user.otp_device = self._verify_token(user, token, device)
            else:
                raise forms.ValidationError(_("Please enter your OTP token."), code="required")
            FloodLog.objects.reset_target(self.request, flood_target)

        except forms.ValidationError:
            FloodLog.objects.add_record(self.request, flood_target)
            raise
        except FloodOverflowError:
            raise forms.ValidationError(_("To many failed 2FA attempts."), code="flood_overflow")
        finally:
            if user.otp_device is None:
                self._update_form(user)


class BackupTokenForm(OTPTokenForm):
    otp_token = forms.CharField(label=_("Token"), widget=forms.TextInput(attrs={"autofocus": True}))
    otp_device = forms.ChoiceField(choices=[], required=False)


class TOTPDeviceForm(TOTPDeviceForm):
    token = forms.IntegerField(label=_("Token"), widget=forms.TextInput(attrs={"autofocus": True}))


class ChangePasswordForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user", None)
        super().__init__(*args, **kwargs)

    old_password = forms.CharField(
        strip=False,
        widget=forms.PasswordInput(attrs={"autofocus": True, "autocomplete": "current-password"}),
    )

    new_password1 = forms.CharField(strip=False, widget=forms.PasswordInput(attrs={"autocomplete": "new-password"}))
    new_password2 = forms.CharField(strip=False, widget=forms.PasswordInput(attrs={"autocomplete": "new-password"}))

    def clean_old_password(self):
        old_password = self.cleaned_data.get("old_password")
        user = authenticate(username=self.user.email, password=old_password)

        if user is None:
            raise forms.ValidationError(
                messages.PASSWORD_INVALID,
                code="invalid_password",
            )

        return old_password

    def clean_new_password2(self):
        new_password1 = self.cleaned_data.get("new_password1")
        new_password2 = self.cleaned_data.get("new_password2")

        if new_password1 and new_password2 and new_password1 != new_password2:
            raise forms.ValidationError(
                messages.PASSWORD_MISMATCH,
                code="password_mismatch",
            )

        password_validation.validate_password(self.cleaned_data.get("new_password2"))
        return new_password2


class PageForm(forms.ModelForm):
    class Meta:
        model = Page
        fields = ("page_text",)


class PasswordResetForm(PasswordResetFormBase, RecaptchaForm):
    def __init__(self, request, *args, **kwargs):
        kwargs["add_reCAPTCHA"] = getattr(settings, "GOOGLE_RECAPTCHA_SITE_KEY", None) and EventLog.recaptcha_needed(
            request
        )
        super().__init__(request, *args, **kwargs)


class RegisterActivateForm(RecaptchaForm):
    def __init__(self, request, *args, **kwargs):
        kwargs["add_reCAPTCHA"] = getattr(settings, "GOOGLE_RECAPTCHA_SITE_KEY", None)
        super().__init__(request, *args, **kwargs)
