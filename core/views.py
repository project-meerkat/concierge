import logging
from base64 import b32encode
from binascii import unhexlify
from urllib.parse import ParseResult, urlparse, urlunparse

import django_otp
from django.conf import settings
from django.contrib import auth, messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ValidationError
from django.core.signing import Signer
from django.db import IntegrityError
from django.http import Http404, HttpResponseNotFound, HttpResponseRedirect
from django.shortcuts import redirect, render
from django.urls import reverse
from django.utils import timezone
from django.utils.http import urlencode
from django.utils.translation import gettext_lazy as _
from django_otp.util import random_hex
from oidc_provider.models import Client
from oidc_provider.views import EndSessionView
from two_factor.views import DisableView

from core.oidc.endpoints import is_valid_redirect
from floodctl.exceptions import FloodOverflowError
from floodctl.models import FloodLog
from floodctl.utils import flood_protection
from saml.models import SamlIdentityProvider

from .class_views import BackupTokensView, BaseLoginView, ProfileView, SessionListView
from .forms import (
    CompleteProfileForm,
    RegisterActivateForm,
    RegisterForm,
    TOTPDeviceForm,
    UserProfileForm,
)
from .helpers import constance_value, set_origin_cookie
from .lib import Reverse, force_same_site_url
from .mailers import (
    send_account_recovery_email,
    send_activation_token,
    send_change_email_activation_token,
    send_delete_account_confirm_code,
    send_password_reset_link,
)
from .models import (
    EventLog,
    Page,
    PendingUser,
    User,
    UserSiteRegistration,
    ValidCountry,
)

logger = logging.getLogger(__name__)


def home(request):
    if request.user.is_authenticated:
        return redirect(settings.LOGIN_REDIRECT_URL)

    return redirect(settings.LOGIN_URL)


def logout(request):
    # If user has logged in via SAML IDP verification, the user has to log out
    # at SAML IDP first if the IDP supports / allows that
    # IdentityProvider.sloId either contains url for single logout or is empty
    if request.session.pop("samlLogin", None) or request.session.pop("samlConnect", None):
        slo = None
        idp = request.session.get("idp", None)
        if idp:
            idp_row = SamlIdentityProvider.objects.get(shortname=idp)
            if idp_row.perform_slo:
                try:
                    slo = idp_row.sloId
                except SamlIdentityProvider.DoesNotExist:
                    slo = None
            else:
                slo = None
        if slo:
            request.session["slo"] = "slo"
            return redirect("saml_slo")

    oidc_logout = request.session.pop("oidc_logout", None)
    if oidc_logout:
        return redirect(oidc_provider_logout_url(request, oidc_logout))

    EventLog.add_event(request, EventLog.USER_LOGGED_OUT, None)

    # If needed the account can be deleted now that user has been logged out at
    # SAML IDP
    if request.session.pop("DeleteAccountPending", None):
        EventLog.add_event(request, EventLog.ACCOUNT_DELETED, None)
        request.user.delete()

    auth.logout(request)

    return redirect(get_post_logout_url(request))


def oidc_provider_logout_url(request, base_url):
    return_url = request.build_absolute_uri()
    query_params = urlencode({"post_logout_redirect_uri": return_url})
    return f"{base_url}?{query_params}"


def get_post_logout_url(request):
    client_id = request.GET.get("client_id", None)
    post_logout_redirect_uri = request.GET.get("post_logout_redirect_uri", get_legacy_post_logout_url(request))

    if client_id:
        try:
            client = Client.objects.get(client_id=client_id)
            if client.post_logout_redirect_uris:
                if is_valid_redirect(client.post_logout_redirect_uris, post_logout_redirect_uri):
                    return post_logout_redirect_uri
        except Client.DoesNotExist:
            pass

    return settings.LOGIN_URL


def get_legacy_post_logout_url(request):
    # This is the way the original logout function decided on post_logout_url
    # Likely it did not support the post_logout_redirect_uri, whether there are websites that rely on this is unknown
    try:
        http_referer = urlparse(request.META["HTTP_REFERER"])
        clean_url = urlunparse(
            ParseResult(
                scheme=http_referer.scheme,
                netloc=http_referer.netloc,
                path="",
                params="",
                query="",
                fragment="",
            )
        )

        return clean_url
    except Exception:
        return settings.LOGIN_URL


def oidc_logout(request):
    EndSessionView.as_view()(request)
    return logout(request)


def register(request, email=None):
    if email and "@" not in email:
        return HttpResponseNotFound()

    if request.user.is_authenticated:
        return redirect(settings.LOGIN_REDIRECT_URL)

    page_text = Page.get_page(request, page_name="terms")

    if request.method == "POST":
        reverse_url = Reverse(request, override_parameters={"username": request.POST.get("email")})
        if "button_login" in request.POST:
            return redirect(reverse_url("login"))

        form = RegisterForm(request=request, data=request.POST)
        if ValidCountry.valid_country(request.session.ip):
            if form.is_valid():
                data = form.cleaned_data
                try:
                    user = User.objects.create_user(
                        name=data["name"],
                        email=data["email"].lower(),
                        password=data["password2"],
                        accepted_terms=data["accepted_terms"],
                    )
                except IntegrityError:
                    user = User.objects.get(email__iexact=data["email"])

                if user.is_active:
                    send_account_recovery_email(user)
                else:
                    pending_user = PendingUser.objects.create(
                        user=user,
                        next=force_same_site_url(request, request.GET.get("next")),
                    )
                    send_activation_token(user, pending_user)
                    EventLog.add_event(request, EventLog.USER_REGISTERED, user)

                return redirect(reverse_url("register_complete"))
        else:
            EventLog.add_event(request, EventLog.INVALID_COUNTRY_CODE, None)
            messages.error(request, _("Can't process request"), extra_tags="cant_proceed")
    else:
        set_origin_cookie(request)

        form = RegisterForm(
            request=request,
            initial={
                "email": request.GET.get("username") or "",
                "next": request.GET.get("next", ""),
            },
        )

    return render(request, "register.html", {"form": form, "page_text": page_text})


def register_complete(request):
    if request.method == "POST":
        reverse_url = Reverse(request)
        return redirect(reverse_url("login"))
    return render(request, "register_complete.html")


def register_activate(request, activation_token):
    if request.user.is_authenticated:
        return redirect(reverse("dashboard"))

    try:
        pending_user = PendingUser.objects.get(pk=activation_token)
        pending_user.assert_not_due()
        site = pending_user.get_origin_site()
    except (ValidationError, PendingUser.DoesNotExist):
        pending_user = None
        site = None

    context = {
        "activation_token": activation_token,
        "site_name": site.origin_site_name if site else constance_value("SITE_TITLE"),
    }
    if pending_user:
        if request.method == "POST":
            form = RegisterActivateForm(request, request.POST)

            if form.is_valid():
                pending_user.activate_user()
                redirect_url = pending_user.next or reverse("dashboard")

                EventLog.add_event(request, EventLog.REGISTRATION_CONFIRMED)
                auth.login(
                    request,
                    pending_user.user,
                    backend="django.contrib.auth.backends.ModelBackend",
                )
                EventLog.add_event(request, EventLog.USER_LOGGED_IN)

                PendingUser.objects.cleanup(user=pending_user.user)
                return redirect(redirect_url)
            else:
                # Re-view the form.
                context["form"] = form
        else:
            # View the form.
            context["form"] = RegisterActivateForm(request)
    else:
        # did not find the token in the database.
        pass

    return render(request, "register_activate.html", context)


@login_required
def complete_profile(request):
    user: User = request.user

    if user.is_authenticated and user.accepted_terms:
        return redirect(reverse("profile"))

    if request.method == "POST":
        ready = bool("complete_profile" in request.POST)
        form = CompleteProfileForm(data=request.POST, files=request.FILES, require_terms=ready, instance=user)
        if form.is_valid():
            form.save()
            user.process_avatar()
            user.update_avatar_name()
            if ready:
                return redirect(request.GET.get("next") or reverse("dashboard"))
    else:
        form = CompleteProfileForm(instance=user)

    return render(
        request,
        "register_update_profile.html",
        {
            "form": form,
            "user": user,
        },
    )


def send_new_email_activation(request):
    user: User = request.user
    send_change_email_activation_token(user)
    messages.info(request, _("Email address changed"), extra_tags="email_sent")
    EventLog.add_event(request, EventLog.EMAIL_CHANGE_PENDING)

    return redirect(settings.LOGIN_REDIRECT_URL)


def change_email_activate(request, activation_token=None):
    user = User.change_email(None, activation_token)

    if user:
        messages.success(request, _("Email address changed"), extra_tags="email")
        EventLog.add_event(request, EventLog.EMAIL_CHANGE_CONFIRMED)

    return redirect(settings.LOGIN_REDIRECT_URL)


@login_required
def profile(request):
    if request.method == "POST":
        new_email_save = request.user.new_email
        form = UserProfileForm(request.POST, request.FILES, instance=request.user)

        if form.is_valid():
            data = form.cleaned_data
            new_email = data["new_email"]
            user = form.save()
            user.new_email = new_email
            user.save()

            user.process_avatar()
            user.update_avatar_name()

            if new_email and (new_email != new_email_save):
                send_new_email_activation(request)

            messages.success(request, _("Profile changed"), extra_tags="profile")

            from core.tasks import profile_updated_signal

            profile_updated_signal.delay(user.id)

            return HttpResponseRedirect(reverse("profile"))

    else:
        form = UserProfileForm(instance=request.user)

    if request.user.new_email:
        text_change_pending = _("There is a pending change of your email to ")
    else:
        text_change_pending = None

    return render(
        request,
        "profile.html",
        {
            "form": form,
            "text_change_pending": text_change_pending,
            "text_email": request.user.new_email,
        },
    )


@login_required
def show_dashboard(request):
    sites = UserSiteRegistration.objects.filter(user_id=request.user)
    return render(request, "dashboard.html", {"sites": sites})


def pages(request, page_name=None, language_code=None):
    page_text = Page.get_page(request, page_name=page_name, language_code=language_code)

    return render(request, "page.html", {"page_text": page_text})


@login_required
def security_pages(request, page_action=None):
    return render(
        request,
        "security_pages.html",
        {
            "pass_reset_form": change_password_form(request, page_action),
            "2FA": two_factor_form(request, page_action),
            "user_session_form": user_sessions_form(request),
        },
    )


def change_password_form(request, page_action):
    if page_action == "change-password":
        pending_user, created = PendingUser.objects.get_or_create(user=request.user, type=PendingUser.PENDING_PWD_RESET)

        if created or (pending_user.created_at < timezone.now() - timezone.timedelta(minutes=15)):
            pending_user.created_at = timezone.now()
            pending_user.next = reverse("security_pages")
            pending_user.save()
            send_password_reset_link(pending_user)

        messages.success(
            request,
            _("Instructions on how to reset your password have been sent to your email address."),
        )

    return None


def two_factor_form(request, page_action):
    two_factor_authorization = {}
    if page_action == "2fa-setup":
        key = random_hex(20)
        rawkey = unhexlify(key.encode("ascii"))
        b32key = b32encode(rawkey).decode("utf-8")

        request.session["tf_key"] = key
        request.session["django_two_factor-qr_secret_key"] = b32key

        two_factor_authorization = {
            "form": TOTPDeviceForm(key=key, user=request.user),
            "QR_URL": reverse("two_factor:qr"),
        }
        two_factor_authorization["state"] = "setup"

    elif page_action == "2fa-setupnext":
        key = request.session.get("tf_key")
        form = TOTPDeviceForm(data=request.POST, key=key, user=request.user)
        if form.is_valid():
            device = form.save()
            django_otp.login(request, device)
            two_factor_authorization["default_device"] = True
            two_factor_authorization["show_state"] = True
            EventLog.add_event(request, EventLog.TFA_ENABLED)
        else:
            two_factor_authorization["form"] = form
            two_factor_authorization["QR_URL"] = reverse("two_factor:qr")
            two_factor_authorization["state"] = "setup"

    elif page_action == "2fa-disable":
        two_factor_authorization = DisableView.as_view(template_name="security_pages.html")(request).context_data
        two_factor_authorization["state"] = "disable"

    elif page_action == "2fa-disableconfirm":
        two_factor_authorization = DisableView.as_view(template_name="security_pages.html")(request)
        two_factor_authorization["state"] = "default"
        two_factor_authorization["show_state"] = "true"
        EventLog.add_event(request, EventLog.TFA_DISABLED)

    elif page_action == "2fa-showcodes":
        two_factor_authorization = BackupTokensView.as_view(template_name="backup_tokens.html")(request).context_data
        two_factor_authorization["default_device"] = "true"
        two_factor_authorization["state"] = "codes"
        two_factor_authorization["show_state"] = "true"

    elif page_action == "2fa-generatecodes":
        two_factor_authorization = BackupTokensView.as_view(template_name="security_pages.html")(request).context_data
        two_factor_authorization["default_device"] = "true"
        two_factor_authorization["show_state"] = "true"
        two_factor_authorization["state"] = "codes"

    else:
        two_factor_authorization = ProfileView.as_view(template_name="security_pages.html")(request).context_data
        two_factor_authorization["state"] = "default"
        two_factor_authorization["show_state"] = "true"

    return two_factor_authorization


def user_sessions_form(request):
    user_sessions = SessionListView.as_view(template_name="security_pages.html")(request).context_data

    return user_sessions["object_list"]


@login_required
def initiate_delete_account(request):
    if request.method == "POST":
        signer = Signer()
        data = [request.user.email, int(timezone.now().timestamp())]
        url = reverse("confirm_delete_account", args=[signer.sign_object(data)])

        send_delete_account_confirm_code(request.user, url)
        messages.success(
            request,
            _("Follow the instructions in your mail to have your account removed"),
        )
        return redirect(settings.EXTERNAL_HOST)

    return render(
        request,
        "delete_account.html",
        {
            "email": request.user.email,
        },
    )


def confirm_delete_account(request, code):
    try:
        signer = Signer()
        email, timestamp = signer.unsign_object(code)

        expected = timezone.now() - timezone.timedelta(minutes=60)
        actual = timezone.datetime.fromtimestamp(timestamp, timezone.utc)

        assert expected < actual
        user = User.objects.get(email=email)

        if request.method == "POST":
            messages.success(request, _("Account removed"), extra_tags="account_deleted")

            if request.user.is_authenticated and email == request.user.email:
                request.session["DeleteAccountPending"] = True
                return redirect(settings.LOGOUT_REDIRECT_URL)
            else:
                EventLog.add_event(request, EventLog.ACCOUNT_DELETED, None)
                user.delete()
                return redirect(settings.EXTERNAL_HOST)

        return render(
            request,
            "delete_account_confirm.html",
            {
                "name": user.name,
                "email": user.email,
            },
        )
    except Exception:
        raise Http404()


def password_reset_sent(request):
    EventLog.add_event(request, EventLog.PASSWORD_REQUESTED)
    return render(request, "password_reset_sent.html")


def csrf_failure(request, reason=""):
    return render(request, "csrf_failure.html")


def email_sent(request):
    reverse_url = Reverse(request, ["username", "next"])

    if request.method == "POST" and "button_back" in request.POST:
        return redirect(reverse_url("login", post=False))

    return render(request, "login/email_sent.html", {"username": request.GET.get("username")})


def login_with_email(request, pending_user_id):
    if getattr(request, "user", None) and request.user.is_authenticated:
        return redirect(reverse("dashboard"))

    try:
        with flood_protection(request, pending_user_id):
            pending_user = PendingUser.objects.get(pk=pending_user_id)
            pending_user.assert_not_due()

            if request.method == "POST":
                # This action is solid proof of access to the known email address.
                pending_user.activate_user()

                next_url = pending_user.next or reverse("dashboard")
                pending_user.delete()

                blv = BaseLoginView()
                return blv.post_login_process(request, pending_user.user, saml_next=next_url)

            site = pending_user.get_origin_site()
            return render(
                request,
                "login/email_activate.html",
                {
                    "site_name": (site.origin_site_name if site else constance_value("SITE_TITLE")),
                },
            )
    except (PendingUser.DoesNotExist, ValidationError):
        FloodLog.objects.add_record(request, pending_user_id)
    except FloodOverflowError as e:
        e.report(request)

    return redirect(reverse("login"))
