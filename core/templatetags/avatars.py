from django import template

register = template.Library()


@register.simple_tag
def avatar_url(user, size):
    if not user.avatar.name:
        return user.avatar_url
    return f"{user.avatar_url}?size={size}"
