from django import template
from django.contrib.staticfiles import finders
from django.utils.safestring import mark_safe

register = template.Library()


@register.simple_tag
def include_asset(file_name):
    result = finders.find(file_name)
    return mark_safe(open(result).read()) if result else ""
